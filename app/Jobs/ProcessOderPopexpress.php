<?php

namespace App\Jobs;

use App\Http\Library\ApiPopExpress;
use App\Http\Models\DeliveryDestination;
use App\Http\Models\District;
use Illuminate\Support\Facades\Cache;
use App\Http\Models\Logs;


class ProcessOderPopexpress extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $destinations;
    public $pickup;
    public function __construct($pickup,$destinations)
    {
        $this->destinations = $destinations;
        $this->pickup = $pickup;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $location = "";
        Logs::logFile($location,"orderExpress","Begin Order Express");
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $popExpressAPI = new ApiPopExpress();

        $pickupOrigin = $this->getPickupOrigin($this->pickup['district_id']);
        $pickup = [
            'account_name' => 'Popsend',
            'pickup_date' => date('Y-m-d',strtotime($this->pickup['pickup_date'])),
            'pickup_time' => date('H:i',strtotime($this->pickup['pickup_date'])),
            'pickup_location' => $this->pickup['origin_name'],
            'pickup_alternative_location' => $this->pickup['origin_address'],
            'pickup_type' => $this->pickup['pickup_type'],
            'origin' => $pickupOrigin->pickupCity,
            'origin_code' => $pickupOrigin->pickupCode,
            'sender_name' => $this->pickup['name_sender']
        ];

        Logs::logFile($location,"orderExpress","Pickup Data : ".json_encode($pickup));

        foreach ($this->destinations as $i => $item) {
            $districtDb = District::find($item->district_id);
            $districtCode = $districtDb->code;
            //insert order to popexpress
            $destinations = [
                'customer_order_no' => $item->invoice_sub_code,
                'sender_telp' => $item->phone_recipient,
                'notes' => (!empty($item->notes)) ? $item->notes: '-',
                'weight' => $item->weight,
                'service_type' => 'regular',
                'destination_type' => $item->destination_type,
                'destination_code' => $districtCode,
                'recipient_address' => $item->destination_name,
                'recipient_email_address' => '',
                'recipient_telp' => $item->phone_recipient,
                'recipient_name' => $item->name_recipient,
                'parcel_price' => $item->price,
                'insurance' => 0,
                'reseller' => $this->pickup['name_sender']
            ];

            Logs::logFile($location,"orderExpress","Destination Data : ".json_encode($destinations));

            $this->saveParam($item->id,$pickup,$destinations);

            $expressOrder = $popExpressAPI->pickupRequestMarketplace($pickup,[$destinations]);

            Logs::logFile($location,"orderExpress", "Response : ".json_encode($expressOrder));

            if ($expressOrder->response->code == 400) {
                Logs::logFile($location,"orderExpress","Failed : ".$expressOrder->response->message);

                $response->errorMsg = $expressOrder->response->message;
                return;
            }
            //update delivery
            $delivery = DeliveryDestination::find($item->id);
            $delivery->pickup_number = $expressOrder->data->pickup_request_number;
            $delivery->save();
        }


        $response->isSuccess = true;
        $response->pickup_request_number = $expressOrder->data->pickup_request_number;
        Logs::logFile($location,"orderExpress","Success : ".json_encode($response));
        
        return;
    }

    private function getPickupOrigin ($districtId)
    {
        $response = new \stdClass();
        $response->pickupCity = null;
        $response->pickupCode = null;
        // initiate model
        $pickupCity = null;
        $pickupDistrictId = null;
        $pickupCode = null;

        $districtDb = District::with('city')
            ->where('id',$districtId)
            ->first();
        if ($districtDb) {
            $pickupCity = $districtDb->city->city_name;
        }

        $popExpressAPI = new ApiPopExpress();

        // check origin pickup
        $cityOrigin = Cache::get('express-city-origin',null);
        if (empty($cityOrigin)){
            // get from API
            $getCity = $popExpressAPI->getCityOrigin();
            $cityOrigin = $getCity->data->origins;
            Cache::put('express-city-origin',$cityOrigin,3600);
        }

        $isOrigin = null;
        foreach ($cityOrigin as $item) {
            $tmpName = strtoupper($item->name);
            $tmpCity = strtoupper($pickupCity);
            if (strpos($tmpCity,$tmpName) !== false) {
                $response->pickupCity = $item->name;
                $response->pickupCode =$item->code;
            } elseif (strpos($tmpName,$tmpCity) !== false) {
                $response->pickupCity = $item->name;
                $response->pickupCode =$item->code;
            }
        }

        return $response;
    }

    private function saveParam ($destinationId,$pickup,$destinations){
        $param = [];
        $param['account_name'] = 'Popsend';
        $param['pickup_date'] = $pickup['pickup_date'];
        $param['pickup_time'] = $pickup['pickup_time'];
        $param['pickup_location'] = $pickup['pickup_location'];
        $param['pickup_alternative_location'] = $pickup['pickup_alternative_location'];
        $param['pickup_type'] = $pickup['pickup_type'];
        $param['origin'] = $pickup['origin'];
        $param['origin_code'] = $pickup['origin_code'];
        $param['data'] = [$destinations];
        $param['reseller'] = $pickup['sender_name'];
        $param['api_key'] = env('POP_EXPRESS_KEY');
        $param['account_name'] = 'Popsend';

        $urlExpress = env('POP_EXPRESS_URL').'/pickup_request/marketplace/create';

        // save
        $delivery = DeliveryDestination::find($destinationId);
        $delivery->express_url = $urlExpress;
        $delivery->express_parameter = json_encode($param);
        $delivery->save();
    }
}
