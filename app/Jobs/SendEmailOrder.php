<?php

namespace App\Jobs;

use App\Http\Library\CurlHelper;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;


class SendEmailOrder extends Job implements  ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data,$userId)
    {
        $user = User::find($userId);
        $data->email = $user->email;
        $data->name = $user->name;
        $data->phone = $user->phone;
        $this->data = $data;
    }

    /**
     * handle
     * @param $data
     * @param $userId
     */
    public function handle()
    {
        $data = ['data' => $this->data];

        //send SMS
        $endpintApiSms = env('URL_INTERNAL','https://internalapi.popbox.asia').'/sms/send';
        $destinations = $this->data->destinations;

        if (is_array($destinations)) {
            foreach ($destinations as $destination) {

                $dataSms = [
                    'to' => $this->data->phone,
                    'message' => 'Hi '.$this->data->name.', Order Popsend Anda Sukses, Gunakan Kode PIN : '
                        .$destination->sub_invoice_id
                        .' Untuk Membuka Locker Anda',
                    'token' => env('TOKEN_INTERNAL','0weWRasJL234wdf1URfwWxxXse304')
                ];
                CurlHelper::callCurl($endpintApiSms,$dataSms,'POST');

            }
        }

        $output = PDF::loadView('pdf.invoicepopsend', $data);
        Mail::send('email.popsend.ordercreated',$data, function($message) use ($data,$output){
            $message->subject('PopSend Invoice #'.$this->data->invoice_id);
            $message->to( $data['data']-> email,$data['data']->name);
            $message->from('no-reply@popbox.asia','PopSend');
            $message->attachData($output->output(),$this->data->invoice_id.'.pdf');
        });
    }
}


