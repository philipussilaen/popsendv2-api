<?php

namespace App\Jobs;

use App\Http\Library\Helper;
use App\Http\Models\Delivery;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class SendSMSEmailV2 extends Job
{
    private $deliveryDb;
    private $data;
    /**
     * ProcessExpressV2 constructor.
     * @param Delivery $deliveryDb
     * @param $calculateData
     */
    public function __construct(Delivery $deliveryDb,$calculateData)
    {
        $this->deliveryDb = $deliveryDb;
        $userDb = User::find($deliveryDb->user_id);
        $calculateData->email = $userDb->email;
        $calculateData->phone = $userDb->phone;
        $calculateData->name = $userDb->name;
        $this->data = $calculateData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $deliveryDb = $this->deliveryDb;
        $senderPhone = $deliveryDb->phone_sender;
        $senderName = $deliveryDb->name_sender;
        
        $destinations = $deliveryDb->destinations;

        foreach ($destinations as $item) {
            $invoiceCode = $item->invoice_sub_code;
            $destinationType = $item->destination_type;

            if ($destinationType != 'locker') continue;
            $message = "Hi $senderName. Order PopSend Anda Sukses. Gunakan Kode PIN: $invoiceCode Untuk Membuka Loker Anda";
            // send SMS
            Helper::sendSms($senderPhone,$message);
        }

        // send email
        $data = ['data' => $this->data];

        $output = PDF::loadView('pdf.invoicepopsend', $data);
        Mail::send('email.popsend.ordercreated',$data, function($message) use ($data,$output){
            $message->subject('PopSend Invoice #'.$this->data->invoice_id);
            $message->to( $data['data']-> email,$data['data']->name);
            $message->from('no-reply@popbox.asia','PopSend');
            $message->attachData($output->output(),$this->data->invoice_id.'.pdf');
        });
    }
}
