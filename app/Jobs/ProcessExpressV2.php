<?php

namespace App\Jobs;

use App\Http\Library\ApiPopExpress;
use App\Http\Models\Delivery;
use App\Http\Models\DeliveryDestination;
use App\Http\Models\District;
use App\Http\Models\Logs;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

class ProcessExpressV2 extends Job
{
    private $deliveryDb;

    /**
     * ProcessExpressV2 constructor.
     * @param Delivery $deliveryDb
     */
    public function __construct(Delivery $deliveryDb)
    {
        $this->deliveryDb = $deliveryDb;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $environment = App::environment();
        $location = "";
        Logs::logFile($location,"orderExpressV2","Begin Order Express V2");
        $deliveryDb = $this->deliveryDb;

        $districtDb = District::with('city')->find($deliveryDb->district_id);
        $districtName = $districtDb->district_name;
        $cityName = $districtDb->city->city_name;
        $provinceName = $districtDb->city->province->province_name;

        // create param for API Express
        $param = [];
        $param['pickup_date'] = date('Y-m-d',strtotime($deliveryDb->pickup_date));
        $param['pickup_time'] = date('H:i',strtotime($deliveryDb->pickup_date));
        $param['pickup_type'] = $deliveryDb->pickup_type;
        $param['origin_code'] = $this->getPickupOriginCode($deliveryDb->district_id);
        $param['pickup_main_area'] = $deliveryDb->origin_address_detail;
        $param['pickup_main_street'] = $deliveryDb->origin_name;
        $param['pickup_main_district'] = $districtName;
        $param['pickup_main_city'] = $cityName;
        $param['pickup_main_province'] = $provinceName;

        if($param['pickup_type'] == "locker"){
            $param['pickup_main_area'] = "";
            $param['pickup_main_street'] = $deliveryDb->origin_name;
        }else{
            $param['pickup_main_street'] = $deliveryDb->origin_address_detail;
            $param['pickup_main_area'] = $deliveryDb->origin_name;
            if($deliveryDb->origin_address != ""){
                $param['pickup_main_area'] .= ", "; 
            }
            $param['pickup_main_area'] .= $deliveryDb->origin_address;
        }

        Logs::logFile($location,"orderExpressV2","Pickup Data : ".json_encode($param));

        $destinations = $deliveryDb->destinations;

        foreach ($destinations as $item){
            $param['data'] = [];

            $districtDb = District::with('city')->find($item->district_id);
            $districtName = $districtDb->district_name;
            $cityName = $districtDb->city->city_name;
            $provinceName = $districtDb->city->province->province_name;
            $districtCode = $districtDb->code;

            $destinationStreet = $item->destination_address;
            if ($item->destination_type == 'locker') $destinationStreet = $item->destination_name;

            $destinationParam = [];
            $destinationParam['sender_telp'] = $deliveryDb->phone_sender;
            $destinationParam['weight'] = $item->weight;
            $destinationParam['notes'] = $item->notes;
            $destinationParam['customer_order_no'] = $item->invoice_sub_code;
            $destinationParam['service_type'] = 'regular';
            $destinationParam['destination_type'] = $item->destination_type;
            $destinationParam['destination_code'] = $districtCode;
            $destinationParam['destination_area'] = $item->destination_address_detail;
            $destinationParam['destination_street'] = $destinationStreet;
            $destinationParam['destination_district'] = $districtName;
            $destinationParam['destination_city'] = $cityName;
            $destinationParam['destination_province'] = $provinceName;
            $destinationParam['recipient_email_address'] = 'it@popbox.asia';
            $destinationParam['recipient_telp'] = $item->phone_recipient;
            $destinationParam['recipient_name'] = $item->name_recipient;
            $destinationParam['insurance'] = 0;
            $destinationParam['parcel_price'] = 0;
            $destinationParam['reseller'] = $deliveryDb->name_sender;
            $destinationParam['description'] = $item->category;
            $destinationParam['destination_postcode'] = "";

            if($destinationParam['destination_type'] == "locker"){
                $destinationParam['destination_area'] = "";
                $destinationParam['destination_street'] = $destinationStreet;
            }else{
                $destinationParam['destination_street'] = $item->destination_address_detail;
                $destinationParam['destination_area'] = $item->destination_name;
                if($item->destination_address != ""){
                    $destinationParam['destination_area'] .= ", "; 
                }
                $destinationParam['destination_area'] .= $item->destination_address;
            }

            Logs::logFile($location,"orderExpressV2","Destination Data : ".json_encode($destinationParam));

            $param['data'][] = $destinationParam;

            // push to api express
            Logs::logFile($location,"orderExpressV2","Push Express Param : ".json_encode($param));
            $this->saveParam($item->id,$param);
            $apiExpress = new ApiPopExpress();
            $createV2 = $apiExpress->pickupRequestMarketplaceV2($param);
            Logs::logFile($location,"orderExpressV2","Response Express : ".json_encode($createV2));
            if (empty($createV2)){
                // send notification email
                $data = [];
                $data['subInvoiceCode'] = $item->invoice_sub_code;
                $data['parameter'] = $param;
                $data['errorMsg'] = "Empty Response";

                Mail::send('email.popsend.failed_push_express',$data, function($message) use ($item,$environment){
                    $message->subject("[$environment]".' Failed Push Express Sub Invoice : '.$item->invoice_sub_code);
                    $message->to( 'it@popbox.asia','IT PopBox');
                    $message->from('admin@popbox.asia','PopSend');
                });
                continue;
            }
            if ($createV2->response->code != 200) {
                Logs::logFile($location,"orderExpress","Failed : ".$createV2->response->message);
                $errorMsg = $createV2->response->message;
                // send notification email
                $data = [];
                $data['subInvoiceCode'] = $item->invoice_sub_code;
                $data['parameter'] = $param;
                $data['errorMsg'] = $errorMsg;

                Mail::send('email.popsend.failed_push_express',$data, function($message) use ($item,$environment){
                    $message->subject("[$environment]".' Failed Push Express Sub Invoice : '.$item->invoice_sub_code);
                    $message->to( 'it@popbox.asia','IT PopBox');
                    $message->from('admin@popbox.asia','PopSend');
                });
                continue;
            }

            //update delivery
            $delivery = DeliveryDestination::find($item->id);
            $delivery->pickup_number = $createV2->data->pickup_request_number;
            $delivery->save();
        }

    }

    private function getPickupOriginCode($districtId)
    {
        // initiate model
        $originCode = null;
        $pickupCity = null;

        $districtDb = District::with('city')
            ->where('id',$districtId)
            ->first();
        if ($districtDb) {
            $pickupCity = $districtDb->city->city_name;
        }

        $popExpressAPI = new ApiPopExpress();

        // check origin pickup
        $cityOrigin = Cache::get('express-city-origin',null);
        if (empty($cityOrigin)){
            // get from API
            $getCity = $popExpressAPI->getCityOrigin();
            $cityOrigin = $getCity->data->origins;
            Cache::put('express-city-origin',$cityOrigin,3600);
        }

        foreach ($cityOrigin as $item) {
            $tmpName = strtoupper($item->name);
            $tmpCity = strtoupper($pickupCity);
            if (strpos($tmpCity,$tmpName) !== false) {
                $originCode = $item->code;
            }
        }

        return $originCode;
    }

    private function saveParam($destinationId,$param){
        $param['api_key'] = env('POP_EXPRESS_KEY');
        $param['account_name'] = 'Popsend';
        $urlExpress = env('POP_EXPRESS_URL').'/pickup_request/marketplace_2/create';

        $delivery = DeliveryDestination::find($destinationId);
        $delivery->express_url = $urlExpress;
        $delivery->express_parameter = json_encode($param);
        $delivery->save();
    }
}
