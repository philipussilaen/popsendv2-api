<?php

namespace App\Console\Commands;

use App\Http\Models\Logs;
use Illuminate\Console\Command;

class DatabaseRemoveOldLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:remove-old-log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Old Logs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $oldDate = date("Y-m-d H:i:s",strtotime("-14 days"));
        echo "Begin Remove Old Logs Older than $oldDate\n";

        Logs::where('created_at','<=',$oldDate)->delete();
    }
}
