<?php

namespace App\Console\Commands;

use App\Http\Models\Delivery;
use App\Http\Models\DeliveryDestination;
use App\Http\Models\PopboxV1\LockerActivity;
use App\Http\Models\PopboxV1\LockerPickup;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PopSendMalaysiaStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'popsend:malaysia-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PopSend Malaysia Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Begin Check PopSend Malaysia Status\n";

        $numberDays = 7;
        $limitDate = date('Y-m-d H:i:s',strtotime("-$numberDays days"));
        echo "Get PopSend order that $numberDays days old - $limitDate\n";

        $deliveryDB = Delivery::with(['destinations'])
            ->whereHas('user',function ($query) {
                $query->where('country','MY');
            })
            ->where('status','<>','COMPLETED')
            ->where('pickup_type','locker')
            ->where('created_at','>=',$limitDate)
            ->get();

        foreach ($deliveryDB as $item) {
            $pickupName = $item->origin_name;
            $destinationDB = $item->destinations;
            foreach ($destinationDB as $value){
                $popSendStatus = null;
                $remarks = null;
                $destinationType = $value->destination_type;
                $invoiceSubCode = $value->invoice_sub_code;
                $destinationName = $value->destination_name;

                echo "Pickup $pickupName, Destination: $destinationType - $invoiceSubCode - $destinationName\n";

                // check on locker activities pickup
                $lockerActivitiesPickupDb = LockerPickup::where('tracking_no',$invoiceSubCode)
                    ->where('storetime','>=',$limitDate)
                    ->first();
                if (!$lockerActivitiesPickupDb){
                    echo "Locker Activities Pickup Not Found\n";
                    continue;
                }
                $lockerName = $lockerActivitiesPickupDb->locker_name;
                $pickupStatus = $lockerActivitiesPickupDb->status;
                $storeTime = $lockerActivitiesPickupDb->storetime;
                $takeTime = $lockerActivitiesPickupDb->taketime;
                if ($pickupStatus == 'IN_STORE') {
                    $popSendStatus = 'ON_PROCESS';
                    $remarks = "Parcel In Store by Customer in $lockerName at $storeTime";
                } elseif ($pickupStatus == 'COURIER_TAKEN' || $pickupStatus == 'OPERATOR_TAKEN'){
                    $popSendStatus = 'ON_PROCESS';
                    $remarks = "Parcel taken by Courier/Operator in $lockerName at $takeTime";
                } else {
                    echo "Unknown Status $pickupStatus\n";
                    continue;
                }
                echo "PopSend Pickup Status : $popSendStatus - $remarks\n";

                // check on locker activities
                $lockerActivityDb = LockerActivity::where('barcode',$invoiceSubCode)
                    ->where('storetime','>=',$limitDate)
                    ->first();
                if (!$lockerActivityDb){
                    echo "Locker Activity Not Found";
                    continue;
                }
                $lockerName = $lockerActivityDb->locker_name;
                $pickupStatus = $lockerActivityDb->status;
                $storeTime = $lockerActivityDb->storetime;
                $takeTime = $lockerActivityDb->taketime;
                $name = $lockerActivityDb->name;

                if ($pickupStatus == 'IN_STORE') {
                    $popSendStatus = 'ON_PROCESS';
                    $remarks = "Parcel In Store by Courier / Operator by $name in $lockerName at $storeTime";
                }
                elseif ($pickupStatus == 'COURIER_TAKEN' || $pickupStatus == 'OPERATOR_TAKEN'){
                    $popSendStatus = 'ON_PROCESS';
                    $remarks = "Parcel taken by Courier/Operator in $lockerName at $takeTime";
                }
                elseif ($pickupStatus == 'CUSTOMER_TAKEN') {
                    $popSendStatus = 'COMPLETED';
                    $remarks = "Parcel taken by Customer in $lockerName at $takeTime";
                }
                else {
                    echo "Unknown Status $pickupStatus\n";
                    continue;
                }
                echo "PopSend Destination Status : $popSendStatus - $remarks\n";

                if (empty($popSendStatus)){
                    echo "Empty Status $popSendStatus\n";
                    continue;
                }

                // update status
                echo "Update Status $invoiceSubCode $popSendStatus - $remarks\n";
                $destinationModel = new DeliveryDestination();
                DB::beginTransaction();
                $update = $destinationModel->updateDeliveryStatus($invoiceSubCode,$popSendStatus,$remarks);
                if (!$update->isSuccess){
                    DB::rollback();
                    echo "Failed Update Status $update->errorMsg\n";
                    continue;
                }
                DB::commit();
            }
        }
        echo "===Finish===\n";
    }
}
