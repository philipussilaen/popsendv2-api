<?php

namespace App\Console\Commands;

use App\Http\Library\ApiProx;
use App\Http\Models\BalanceRecord;
use App\Http\Models\Popsafe;
use App\Http\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PopSafeAutoExtend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'popsafe:extend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extend PopSafe';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Process Auto Extend Overdue PopSafe \n";
        $popSafeDb = Popsafe::where('status','OVERDUE')->get();

        foreach ($popSafeDb as $item){
            $invoiceCode = $item->invoice_code;
            $userId = $item->user_id;
            $autoExtend = $item->auto_extend;
            echo "$invoiceCode - $userId - Auto $autoExtend\n";

            if (empty($autoExtend)){
                echo "Auto Extend is $autoExtend\n";
                continue;
            }

            DB::beginTransaction();
            // extend expired time of popsafes
            $popSafeModel = new Popsafe();
            $extend = $popSafeModel->extendPopSafe($invoiceCode,true);
            if (!$extend->isSuccess){
                DB::rollback();
                $errorMessage = $extend->errorMsg;
                echo "$errorMessage\n";
                continue;
            }
            $popSafeId = $extend->popSafeId;

            // get popsafesDB
            $popSafeDb = Popsafe::find($popSafeId);
            $totalPrice = $popSafeDb->total_price;
            $parcelId = $popSafeDb->parcel_id;
            $expiredDateTime = $popSafeDb->expired_time;
            $numberOfExtend = $popSafeDb->number_of_extend;

            // update popsafes
            $popSafeDb->last_extended_datetime = date('Y-m-d H:i:s');
            $popSafeDb->number_of_extend = $numberOfExtend+1;
            $popSafeDb->save();

            if (empty($parcelId)){
                DB::rollback();
                $errorMessage = 'Failed to Extend. Empty Parcel';
                echo "$errorMessage\n";
                continue;
            }

            // create new transaction
            $transactionModel = new Transaction();
            $remarks = "Extend PopSafe $invoiceCode";
            $createTransaction = $transactionModel->createPopsafeTransaction($userId,$popSafeId,0,$remarks);
            if (!$createTransaction->isSuccess){
                DB::rollback();
                $errorMessage = $createTransaction->errorMsg;
                echo "$errorMessage\n";
                continue;
            }
            $transactionId = $createTransaction->transactionId;

            $transactionDb = $transactionModel->find($transactionId);
            $paidAmount = $transactionDb->paid_amount;

            // debit transaction
            $balanceModel = new BalanceRecord();
            $debitBalance =$balanceModel->debitDeposit($userId,$paidAmount,$transactionId);
            if (!$debitBalance->isSuccess){
                DB::rollback();
                $errorMessage = $debitBalance->errorMsg;
                echo "$errorMessage\n";
                continue;
            }

            // change overdue to prox
            $overdueDate = date('Y-m-d H:i:s',strtotime($expiredDateTime));
            $unixOverdueDateTime = strtotime($overdueDate) * 1000;
            $apiProx = new ApiProx();
            $changeOverdue = $apiProx->changeOverdueDateTime($parcelId,$unixOverdueDateTime);
            if (empty($changeOverdue)){
                DB::rollback();
                $errorMessage = 'Failed to Extend on locker';
                echo "$errorMessage\n";
                continue;
            }
            if ($changeOverdue->response->code!=200){
                DB::rollback();
                $errorMessage = $changeOverdue->response->message;
                echo "$errorMessage\n";
                continue;
            }
            echo "SET $invoiceCode EXTEND";
            DB::commit();
        }
    }
}
