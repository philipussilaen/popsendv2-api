<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantDiscountRate extends Model
{
    protected $table = 'merchant_discount_rate';
}
