<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryList extends Model
{
    protected $table = 'category_lists';
}
