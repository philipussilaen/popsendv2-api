<?php

namespace App\Http\Models\PopExpress;

use Illuminate\Database\Eloquent\Model;

class Locker extends Model
{
    protected $connection = 'express';
    protected $table = 'lockers';

    public function getLockerByLockerId($lockerId=null){
        $lockerData = null;
        if (empty($lockerId)){
            return $lockerData;
        }

        $lockerDb = self::join('international_codes','international_codes.id','=','lockers.international_id')
            ->where('lockers.id',$lockerId)
            ->select('lockers.id','lockers.name','lockers.address','lockers.address2','lockers.latitude','lockers.longitude','international_codes.province','international_codes.county','international_codes.district')
            ->first();
        return $lockerDb;
    }
}
