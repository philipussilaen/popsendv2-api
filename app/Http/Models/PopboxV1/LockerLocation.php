<?php

namespace App\Http\Models\PopboxV1;

use Illuminate\Database\Eloquent\Model;

class LockerLocation extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'locker_locations';
}
