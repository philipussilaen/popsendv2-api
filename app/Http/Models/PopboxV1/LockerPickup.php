<?php

namespace App\Http\Models\PopboxV1;

use Illuminate\Database\Eloquent\Model;

class LockerPickup extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'locker_activities_pickup';
    public $incrementing = false;
}
