<?php

namespace App\Http\Models\PopboxV1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MemberPickup extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_member_pickup';

    /**
     * Get Recipient Order by Phone
     * @param $phone
     * @return \stdClass
     */
    public function getRecipientOrder($phone){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = [];

        $pickupRecipientDb = self::join('locker_activities_pickup','locker_activities_pickup.tracking_no','=','tb_member_pickup.invoice_id')
            ->where('tb_member_pickup.recipient_phone',$phone)
            ->get();

        $listOrder = [];

        foreach ($pickupRecipientDb as $item) {
            $id = $item->invoice_id;
            $lockerName = null;
            if (!empty($item->recipient_locker_name)){
                $lockerName = $item->recipient_locker_name;
            }
            $lastUpdate = null;
            $pin = null;
            // generate status based on popsend type
            $invoiceId = $item->invoice_id;
            $type = substr($invoiceId,0,3);
            $status = $item->status;
            $lastUpdate = $item->storetime;

            // check on process status ON PROCESS
            if ($item->status == 'IN_STORE') continue;
            if ($item->status == 'COURIER_TAKEN' || $item->status == 'OPERATOR_TAKEN') {
                $status = 'ON PROCESS';
                $lastUpdate = $item->taketime;
            }

            if ($type=='PLL'){
                // check on locker activities
                $lockerActivitiesDb = DB::connection('popbox_db')->table('locker_activities')
                    ->where('barcode',$invoiceId)
                    ->first();
                if ($lockerActivitiesDb){
                    $pin = $lockerActivitiesDb->validatecode;
                    if ($lockerActivitiesDb->status=='IN_STORE'){
                        $status = 'READY FOR PICKUP';
                        $lastUpdate = $lockerActivitiesDb->storetime;
                    } elseif ($lockerActivitiesDb->status == 'OPERATOR_TAKEN' || $lockerActivitiesDb->status == 'COURIER_TAKEN') {
                        $status = 'OVERDUE';
                        $lastUpdate = $lockerActivitiesDb->storetime;
                    }
                    $nowDateTime = date('Y-m-d H:i:s');
                    if ($nowDateTime > $lockerActivitiesDb->overduetime && $lockerActivitiesDb->status=='IN_STORE'){
                        $status = 'OVERDUE';
                        $lastUpdate = $lockerActivitiesDb->overduetime;
                    }
                }
            }

            // check status on return statuses COMPLETED
            $returnStatusDb = DB::connection('popbox_db')->table('return_statuses')
                ->where('number',$invoiceId)
                ->first();
            if ($returnStatusDb){
                if (!empty($returnStatusDb->receiver_name)){
                    $status = 'COMPLETED';
                    $lastUpdate = $returnStatusDb->updated_at;
                }
            }

            $tmp = new \stdClass();
            $tmp->id = (string)$id;
            $tmp->order_number = $invoiceId;
            $tmp->type = 'popsend';
            $tmp->merchant = "PopSend";
            $tmp->locker = $lockerName;
            $tmp->status = $status;
            $tmp->last_update = $lastUpdate;
            $tmp->pin = $pin;
            $listOrder[] = $tmp;
        }

        $response->isSuccess = true;
        $response->data = $listOrder;
        return $response;
    }
}
