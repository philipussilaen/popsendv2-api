<?php

namespace App\Http\Models\PopboxV1;

use Illuminate\Database\Eloquent\Model;

class LockerActivity extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'locker_activities';
    public $incrementing = false;

    /**
     * @param $phone
     * @param array $orderNumber
     * @return \stdClass
     */
    public function getListOrder($phone, $orderNumber=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = [];

        $lockerActivitiesDb = self::leftJoin('tb_merchant_pickup','tb_merchant_pickup.order_number','=','locker_activities.barcode')
            ->where('phone',$phone)
            ->select('locker_activities.*','tb_merchant_pickup.merchant_name','tb_merchant_pickup.order_number')
            ->orderBy('locker_activities.storetime','DESC')
            ->get();

        $listData = [];
        foreach ($lockerActivitiesDb as $item){
            if (in_array($item->barcode,$orderNumber)) continue;
            $id = $item->id;
            $invoiceId = $item->barcode;
            $type = 'delivery';
            $merchantName = 'PopBox';
            if (!empty($item->merchant_name)){
                $type = 'merchant';
                $merchantName = $item->merchant_name;
            }
            $lockerName = $item->locker_name;
            $status = null;
            $lastUpdate = null;
            if ($item->status == 'IN_STORE'){
                $status = 'READY FOR PICKUP';
                $lastUpdate = $item->storetime;
            } elseif ($item->status == 'CUSTOMER_TAKEN'){
                $status = 'COMPLETED';
                $lastUpdate = $item->taketime;
            } elseif ($item->status == 'COURIER_TAKEN'){
                $status = 'COMPLETED';
                $lastUpdate = $item->taketime;
            }
            $pin = $item->validatecode;

            $nowDateTime = date('Y-m-d H:i:s');
            if ($nowDateTime > $item->overduetime && $item->status == 'IN_STORE'){
                $status = 'OVERDUE';
                $lastUpdate = $item->overduetime;
            }

            // put to data and orderNumber Array
            $tmp = new \stdClass();
            $tmp->id = $id;
            $tmp->order_number = $invoiceId;
            $tmp->type = $type;
            $tmp->merchant = $merchantName;
            $tmp->locker = $lockerName;
            $tmp->status = $status;
            $tmp->last_update = $lastUpdate;
            $tmp->pin = $pin;

            $listData[] = $tmp;
        }

        $response->isSuccess = true;
        $response->data = $listData;

        return $response;
    }
}
