<?php

namespace App\Http\Models\PopboxV1;

use Illuminate\Database\Eloquent\Model;

class MemberBalance extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_member_balance';

    /**
     * Get Last Balance
     * @param $phone
     * @return \stdClass
     */
    public function getMemberBalance ($phone)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->lastBalance = 0;
        $response->country = null;

        $country = 'ID';
        $firstDigit = substr($phone,0,2);
        if ($firstDigit == '01'){
            $country = 'MY';
        }
        $lastBalance = 0;
        // get last balance
        $memberBalance = self::where('phone',$phone)->orderBy('id_balance','DESC')->first();
        if ($memberBalance){
            if ($country == 'ID') $lastBalance = $memberBalance->current_balance;
            elseif ($country == 'MY') $lastBalance = $memberBalance->current_balance_myr;
        }

        $response->isSuccess = true;
        $response->lastBalance = $lastBalance;
        $response->country = $country;
        return $response;
    }
}
