<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 28/12/2017
 * Time: 09.06
 */

namespace App\Http\Models;

use App\Http\Library\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserSession extends Model
{
    protected $table = 'user_sessions';

    /**
     * Get UserID by Session ID
     * @param $sessionId
     * @return \stdClass
     */
    public static function getUserBySessionId($sessionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->userId = null;

        $userSessionDb = DB::table('user_sessions')
            ->where('session_id',$sessionId)
            ->first();

        if (!$userSessionDb){
            $response->errorMsg = 'Session Not Found';
            return $response;
        }

        $response->userId = $userSessionDb->user_id;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create User session
     * @param $token
     * @param $memberId
     * @return \stdClass
     */
    public static function createUserSession($token,$memberId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->sessionId = null;

        // get token
        $tokenDb = DB::table('api_tokens')
            ->where('token',$token)
            ->first();

        if (!$tokenDb){
            $response->errorMsg = 'Invalid Token';
            return $response;
        }

        // get user DB
        $userDb = DB::table('users')
            ->where('member_id',$memberId)
            ->first();

        if (!$userDb){
            $response->errorMsg = 'Invalid User';
            return $response;
        }

        // generate session ID
        $randomString = Helper::generateRandomString(25,'AlphaNumUpper');
        $sessionId = encrypt($userDb->member_id."$".$tokenDb->id."$".$randomString);

        // save to DB
        $userSessionDb = new UserSession();
        $userSessionDb->api_token_id = $tokenDb->id;
        $userSessionDb->user_id = $userDb->id;
        $userSessionDb->session_id = $sessionId;
        $userSessionDb->last_activity = time();
        $userSessionDb->save();

        $response->isSuccess = true;
        $response->sessionId = $sessionId;
        return $response;
    }
}