<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class GoogleLocation extends Model
{
    protected $table = 'google_locations';
}
