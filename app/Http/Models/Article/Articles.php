<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 12/06/18
 * Time: 12.07
 */

namespace App\Http\Models\Article;

use App\Http\Library\Helper;
use Illuminate\Database\Eloquent\Model;

class Articles extends Model {

//    protected $connection = 'popsendv2';
//    protected $table = 'articles';

    public function saveData() {

        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $article = new self();

        $article->notification_title = '';
        $article->notification_subtitle = '';
        $article->title = '';
        $article->subtitle = '';
        $article->content = '';
        $article->promo_code = '';
        $article->other_desc = '';
        $article->image_locker = '';
        $article->image_web = '';
        $article->image_mobile = '';
        $article->article_service_id = '';
        $article->article_type_id = '';
        $article->article_status_id = '';
        $article->article_priority = '';
        $article->article_country_id = '';
        $article->start_date = '';
        $article->end_date = '';
        $article->save();

        $response->isSuccess = true;
        $response->data = $article;
        return $response;
    }

    public function getArticle($articleId = null, $filter=[]) {

        $result = null;

        if (!empty($articleId)) {
            $result = $this->articleById($articleId, $filter);
        } else {
            $result = $this->listArticle($filter);
        }

        return $result;
    }

    public function listArticle($filter) {

        $country = Helper::getUserCountry($filter['session_id']);

        $r = null;
        if (empty($country)) {
            return $r;
        }

        $countryName = empty($filter['country']) ? $country : $filter['country'];
        $service = empty($filter['service']) ? null : $filter['service'];
        $status = empty($filter['status']) ? 'publish' : $filter['status'];
        $type = empty($filter['type']) ? null : $filter['type'];

        $article = self::join('countries', 'countries.id', '=', 'articles.article_country_id')
            ->join('article_status', 'article_status.id', '=', 'articles.article_status_id')
            ->join('article_types', 'article_types.id', '=', 'articles.article_type_id')
            ->join('popbox_services', 'popbox_services.id', '=', 'articles.article_service_id')
            ->when($service,function ($query) use ($service){
                $query->where('popbox_services.name',$service);
            })
            ->when($type,function ($query) use ($type){
                $query->where('article_types.name', 'like', '%'. $type .'%');
            })
            ->when($status,function ($query) use ($status){
                $query->where('article_status.name',$status);
            })
            ->when($countryName,function ($query) use ($countryName){
                if (strtolower($countryName) == 'id') {
                    $country = 'Indonesia';
                } else if (strtolower($countryName) == 'my') {
                    $country = 'Malaysia';
                } else {
                    $country = $countryName;
                }
                $query->whereRaw("(countries.country_name = '$country' OR countries.country_name = 'All')");
            })
            ->orderBy('articles.start_date', 'desc')
            ->select('articles.*', 'countries.country_name', 'article_status.name as status', 'article_types.name as types', 'popbox_services.name as services')
            ->get();

        if (empty($article)) {
            return $r;
        }

        $result = $this->formatResult($article);

        return $result;
    }

    public function articleById($articleId = null, $filter) {
        $r = null;
        if (empty($articleId)) {
            return $r;
        }

        $type = empty($filter['type']) ? null : $filter['type'];

        $article = self::join('countries', 'countries.id', '=', 'articles.article_country_id')
            ->join('article_status', 'article_status.id', '=', 'articles.article_status_id')
            ->join('article_types', 'article_types.id', '=', 'articles.article_type_id')
            ->join('popbox_services', 'popbox_services.id', '=', 'articles.article_service_id')
            ->where('articles.id', '=', $articleId)
            ->where('article_types.name' , 'like', '%'. $type .'%')
            ->select('articles.*', 'countries.country_name', 'article_status.name as status', 'article_types.name as types', 'popbox_services.name as services')
            ->first();

        if (empty($article)) {
            return $r;
        }

        $result = $this->formatResult($article);

        return $result;
    }

    private function formatResult($article) {
        $result = null;

        if(is_a($article, 'Illuminate\Database\Eloquent\Collection')) {
            $articleList = [];

            foreach ($article as $item) {
                $tmp = $this->formatStdClass($item);
                $articleList[] = $tmp;
            }

            $result = $articleList;

        } else {
            $tmp = $this->formatStdClass($article);
            $result = $tmp;
        }

        return $result;
    }

    private function formatStdClass($item) {

        $urlForImage = env('URL_PATH');

        $tmp = new \stdClass();
        $tmp->id = $item->id;
        $tmp->priority = $item->article_priority;

        $articleContent = new \stdClass();
        $articleContent->notification_title = $item->notification_title;
        $articleContent->notification_subtitle = $item->notification_subtitle;
        $articleContent->title = $item->title;
        $articleContent->subtitle = $item->subtitle;
        $articleContent->content = $item->content;
        $articleContent->article_code = $item->promo_code;
        $articleContent->other_desc = $item->other_desc;

        $articleImage = new \stdClass();
        $articleImage->image_locker = $urlForImage.$item->image_locker;
        $articleImage->image_web = $urlForImage.$item->image_web;
        $articleImage->image_mobile = $urlForImage.$item->image_mobile;

        $articleDetail = new \stdClass();
        $articleDetail->service = $item->services;
        $articleDetail->status = $item->status;
        $articleDetail->type = $item->types;
        $articleDetail->country = $item->country_name;
        $articleDetail->start_date = $item->start_date;
        $articleDetail->end_date = $item->end_date;

        $tmp->article_content = $articleContent;
        $tmp->article_image = $articleImage;
        $tmp->article_detail = $articleDetail;

        return $tmp;
    }
}