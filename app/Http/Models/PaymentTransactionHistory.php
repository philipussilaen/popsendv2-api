<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentTransactionHistory extends Model
{
    public static function createHistory($transactionId,$totalAmount=0,$status='CREATED'){
        $response = new \stdClass();
        $response->isSucess = false;
        $response->errorMsg = null;

        $data = new self();
        $data->payment_transaction_id = $transactionId;
        $data->total_amount = $totalAmount;
        $data->status = $status;
        $data->save();

        $response->isSucess = true;
        return $response;
    }
}
