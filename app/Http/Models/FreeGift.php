<?php

namespace App\Http\Models;

use App\Http\Library\Helper;
use App\User;
use Illuminate\Database\Eloquent\Model;

class FreeGift extends Model
{
    //
    public function saveData ($request,$answers,$userId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        /*Get data Locker By Locker ID*/
        $lockerModel = new LockerLocation();
        $lockerDb = $lockerModel->getLockerByLockerId($request->input('locker_id'));
        /*End Get data Locker*/
        $freeGift = new self();
        /*Save To Freegift*/
        $freeGift->user_id = $userId;
        $freeGift->merchandise_code = $request->input('sample_code');
        $freeGift->occupation = $request->input('occupation');
        $freeGift->birtd_date = $request->input('birtd_date');
        $freeGift->gender = $request->input('gender');
        $freeGift->invoice_id = 'FG'.Helper::generateRandomString(8);
        $freeGift->locker_name = $lockerDb->name;
        $freeGift->locker_address = $lockerDb->address;
        $freeGift->locker_address_detail = $lockerDb->address_2;
        $freeGift->operation_hours = $lockerDb->operational_hours;
        $freeGift->save();
        if(!empty($answers)) {
            foreach ($answers as $answer) {
                foreach ($answer as $i => $items){
                    /*Save to FreeGift Answer*/
                    $freeGiftAnswer = new FreeGiftAnswer();
                    $freeGiftAnswer->free_gift_id = $freeGift->id;
                    $freeGiftAnswer->question_id = $items['question_id'];
                    $freeGiftAnswer->save();
                    foreach ($items['answers'] as $item) {
                        /*Save to Free Gift Answer Detail*/
                        $freeGiftAnswerDetail = new FreeGiftAnswerDetail();
                        $freeGiftAnswerDetail->free_gift_answer_id = $freeGiftAnswer->id;
                        $freeGiftAnswerDetail->choice_id = $item['choice_id'];
                        $freeGiftAnswerDetail->save();
                    }
                }
            }

            if (!$freeGiftAnswerDetail) {
                $php_errormsg = 'Insert Error';
                $response->errorMsg = $php_errormsg;
                return $response;
            }
        }

        $response->isSuccess = true;
        $response->data = $freeGift;
        return $response;
    }

    public function updateOrder ($id, $dataProx)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $freeGift = self::find($id);
        $freeGift->prox_id = $dataProx->id;
        $freeGift->save();

        $response->isSuccess = true;
        return $response;
    }

    public function getHistory ($user_id)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $history = self::where('user_id','=',$user_id)
            ->select('locker_name','locker_address','locker_address_detail',
            'operation_hours','locker_number','code_pin','invoice_id','status','created_at')
            ->orderby('created_at','desc')
            ->get();
        if (empty($history)) {
            return $response->errorMsg = "Data Not Found";
        }
        $history_ = [];
        foreach ($history as $item) {
            $history_[] = $item;
        }
        $response->isSuccess = true;
        $response->data = $history_;
        return $response;
    }


    /*RELATIONS*/
    public function users ()
    {
        return $this->belongsTo(User::class);
    }
    public function free_gift_answers ()
    {
        return $this->hasMany(FreeGiftAnswer::class, 'free_gift_id', 'id');
    }
}
