<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 09/06/18
 * Time: 23.30
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class LockerAvailabilityReport extends Model {

    protected $connection = 'popbox_db';
    protected $table = 'tb_locker_availability_report';

    public static function getLockerIsOnline($lockerId = null) {
        $lockerData = null;
        if (empty($lockerId)){
            return $lockerData;
        }

        $lockerAvailability= self::where('locker_id',$lockerId)
            ->select('locker_name','status')
            ->first();

        $tmp = new \stdClass();
        $tmp->name = $lockerAvailability->locker_name;
        $tmp->status = $lockerAvailability->status;

        return $tmp;
    }
}