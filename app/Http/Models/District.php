<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';

    public function city(){
        return $this->belongsTo(City::class,'city_id','id');
    }
}
