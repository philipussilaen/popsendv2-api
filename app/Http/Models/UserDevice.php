<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 12/01/2018
 * Time: 17.01
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserDevice extends Model
{
    protected $table = 'user_devices';

    /**
     * Insert user devices data to DB
     * @param $memberId
     * @param string $type
     * @param $gcmToken
     * @param $deviceId
     * @param $deviceName
     * @return \stdClass
     */
    public static function insertUserDevices($memberId,$type='android',$gcmToken,$deviceId,$deviceName){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get user DB
        $userDb = DB::table('users')
            ->where('member_id',$memberId)
            ->first();

        if (!$userDb){
            $response->errorMsg = 'Invalid User';
            return $response;
        }

        // check device id
        $deviceDb = self::where('user_id',$userDb->id)
            ->where('device_id',$deviceId)
            ->where('type',$type)
            ->first();
        if ($deviceDb){
            $deviceDb = self::find($deviceDb->id);
            $deviceDb->gcm_token = $gcmToken;
            $deviceDb->save();
        } else {
            // insert into DB
            $deviceDb = new self();
            $deviceDb->user_id = $userDb->id;
            $deviceDb->type = $type;
            $deviceDb->gcm_token = $gcmToken;
            $deviceDb->device_id = $deviceId;
            $deviceDb->name = utf8_encode($deviceName);
            $deviceDb->save();
        }


        $response->isSuccess = true;
        return $response;
    }
}