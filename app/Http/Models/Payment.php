<?php

namespace App\Http\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Library\PaymentAPI;
class Payment extends Model
{
    //table 
    protected $table = 'payments';

    public function createPayment(Request $request, $paymentMethodId, $transaction, $userDb, $remarks = null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $paymentAmount = $request->input('paymentAmount');
        $reference = $request->input('transactionRecord');
        $method = $request->input('paymentMethod');
        $phone = $request->input('takeUserPhoneNumber');

        $paymentAPI = new PaymentAPI();
        $paymentParam['transaction_id'] = [$reference];
        $paymentDetail = $paymentAPI->inquiryPayment($paymentParam);
        
        $paymentId = null;
        $payment_defined = null;
        if ($paymentDetail->response->code == 200) {
            $paymentId = $paymentDetail->data[0]->payment_id;
            $payment_defined = 1;
        }

        // if (empty($remarks)) $remarks = "[Direct Order] PopSafe $popSafeCode @ $lockerName";
        DB::beginTransaction();
    

        $paymentTrx = new PaymentTransaction();
        $paymentTrx->user_id = $userDb->id;
        $paymentTrx->reference = $reference;
        $paymentTrx->payment_reference = $paymentId;
        $paymentTrx->method = $method;
        $paymentTrx->phone = $phone;
        $paymentTrx->country = $userDb->country;
        $paymentTrx->description = $remarks;
        $paymentTrx->amount = $paymentAmount;
        $paymentTrx->paid_amount = $paymentAmount;
        $paymentTrx->total_amount = $paymentAmount;
        $paymentTrx->status = 'PAID';
        $paymentTrx->payment_defined = $payment_defined;
        $paymentTrx->data_request = $request->getContent();
        $paymentTrx->save();

        if ($paymentTrx->save()) {
            $paymentTransaction = PaymentTransaction::where('reference', $reference)->first();
            $data = new PaymentTransactionHistory();
            $data->payment_transaction_id = $paymentTransaction->id;
            $data->total_amount = $paymentAmount;
            $data->status = "PAID";
            $data->save();

            $payment = new Payment();
            $payment->payment_methods_id = $paymentMethodId;
            $payment->transactions_id = $transaction->transactionId;
            $payment->payment_transactions_id = $paymentTransaction->id;
            $payment->amount = $paymentAmount;
            $payment->status = "PAID";
            $payment->save();
        }
        
        if (!$payment->save() || !$paymentTrx->save() || !$data->save()) {
            DB::rollback();
            return $response;
        }
        
        DB::commit();

        $response->isSuccess = true;
        return $response;

    }

    public function createPaymentExtend($request, $paymentMethodId, $transaction, $userDb, $remarks = null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $paymentAmount = $request->cost_overdue;
        $reference = $request->transactionRecord;
        $method = $request->paymentMethod;
        $phone = $request->takeUserPhoneNumber;

        // if (empty($remarks)) $remarks = "[Direct Order] PopSafe $popSafeCode @ $lockerName";
        DB::beginTransaction();

        $paymentTrx = new PaymentTransaction();
        $paymentTrx->user_id = $userDb->id;
        $paymentTrx->reference = $reference;
        $paymentTrx->method = $method;
        $paymentTrx->phone = $phone;
        $paymentTrx->country = $userDb->country;
        $paymentTrx->description = $remarks;
        $paymentTrx->amount = $paymentAmount;
        $paymentTrx->paid_amount = $paymentAmount;
        $paymentTrx->total_amount = $paymentAmount;
        $paymentTrx->status = 'PAID';
        $paymentTrx->data_request = json_encode($request);
        $paymentTrx->save();

        if ($paymentTrx->save()) {
            $paymentTransaction = PaymentTransaction::where('reference', $reference)->first();
            $data = new PaymentTransactionHistory();
            $data->payment_transaction_id = $paymentTransaction->id;
            $data->total_amount = $paymentAmount;
            $data->status = "PAID";
            $data->save();

            $payment = new Payment();
            $payment->payment_methods_id = $paymentMethodId;
            $payment->transactions_id = $transaction->transactionId;
            $payment->payment_transactions_id = $paymentTransaction->id;
            $payment->amount = $paymentAmount;
            $payment->status = "PAID";
            $payment->save();
        }
        
        if (!$payment->save() || !$paymentTrx->save() || !$data->save()) {
            DB::rollback();
            return $response;
        }
        
        DB::commit();

        $response->isSuccess = true;
        return $response;
    }

    public function savePayment($methodCode, $transaction, $paymentTransaction, $paymentAmount)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $paymentMethod = PaymentMethod::where('code', $methodCode)->first();
        
        $payment = new Payment();
        $payment->payment_methods_id = $paymentMethod->id;
        $payment->transactions_id = $transaction->transactionId;
        $payment->payment_transactions_id = $paymentTransaction->paymentId;
        $payment->amount = $paymentAmount;
        $payment->status = $paymentTransaction->status;
        $payment->save();

        $response->isSuccess = true;
        return $response;
    }

}
