<?php

namespace App\Http\Models;

use App\Http\Library\Helper;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Delivery extends Model
{
    protected $fillable = [
        'user_id','district_id','pickup_type','origin_name',
        'origin_address','origin_lattitude','origin_langitude',
        'origin_locker_size','item_photo','pickup_date','invoice_code',
        'status','phone_sender',
        'name_sender','notes'
    ];

    /**
     * Create Order
     * @param $userId
     * @param $calculateData
     * @param Request $request
     * @return \stdClass
     */
    public function createOrder($userId, $calculateData,Request $request){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->orderId = null;

        // get user ID
        $userDb = User::find($userId);
        if (!$userDb){
            $response->errorMsg = 'Invalid User';
            return $response;
        }
        $userName = $userDb->name;
        $pickupType = $calculateData->pickup_type;
        $districtId = $calculateData->districtId;
        $originName = $calculateData->pickup_name;
        $originAddress = $calculateData->pickup_address;
        $originAddressDetail = $request->input('pickup_address_detail',null);
        $originLatitude = $calculateData->pickup_latitude;
        $originLongitude = $calculateData->pickup_longitude;
        $totalPrice = $calculateData->original_price;
        $originLockerSize = '';
        $itemPhoto = $request->input('item_photo');
        $senderName = $request->input('sender_name');
        $senderPhone = $request->input('sender_phone');

        // CHECK IF NOTES ISEMPTY
        $notes = $request->input('pickup_notes');
        if (empty($notes)) {
            $notes = "-";
        }

        $destinationsData = $calculateData->destinations;
        $destinationInput = $request->input('destinations');

        // create invoice ID
        $invoiceId  = Helper::generateInvoiceCode($pickupType,'origin','',$userDb->country);

        // create pickup date
        $pickupDate = date('Y-m-d H:i:s');
        if (date('H') >= 17){
            $pickupDate = date('Y-m-d 08:00:00',strtotime("+1 days"));
        }

        // save photo for popsend
        $pathPhoto = null;
        if($itemPhoto){
            $fileName = "$invoiceId.jpg";
            $structure = 'media_/popsend_/'.date('Y-M');
            $pathPhoto = $structure."/".$fileName;
        }

        // save to DB
        $deliveryDb = new self();
        $deliveryDb->user_id = $userId;
        $deliveryDb->district_id = $districtId;
        $deliveryDb->pickup_type = $pickupType;
        $deliveryDb->origin_name = $originName;
        $deliveryDb->origin_address = $originAddress;
        $deliveryDb->origin_address_detail = $originAddressDetail;
        $deliveryDb->origin_latitude = $originLatitude;
        $deliveryDb->origin_longitude = $originLongitude;
        $deliveryDb->origin_locker_size = $originLockerSize;
        $deliveryDb->item_photo = $pathPhoto;
        $deliveryDb->pickup_date = $pickupDate;
        $deliveryDb->invoice_code = $invoiceId;
        $deliveryDb->status = 'CREATED';
        $deliveryDb->phone_sender = $senderPhone;
        $deliveryDb->name_sender = $senderName;
        $deliveryDb->notes = $notes;
        $deliveryDb->total_price = $totalPrice;
        $deliveryDb->save();

        $deliveryId = $deliveryDb->id;

        // insert delivery destination
        $deliveryDestinationModel = new DeliveryDestination();
        $insertDestination = $deliveryDestinationModel->insertDestinations($deliveryId,$destinationsData,$destinationInput,$userName,$pickupType,$userDb->country);
        if (!$insertDestination->isSuccess){
            $response->errorMsg = $insertDestination->errorMsg;
            return $response;
        }

        // insert delivery history
        $this->insertDeliveryHistory($deliveryId,'CREATED',$userName);

        $response->isSuccess = true;
        $response->deliveryId = $deliveryId;
        $response->pathPhoto = $pathPhoto;

        return $response;
    }

    /**
     * get history delivery by user Id
     * @param $userId
     * @return \stdClass
     */
    public function historyDelivery ($userId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->orderId = null;

        // get user ID
        $userDb = User::find($userId);
        if (!$userDb){
            $response->errorMsg = 'Invalid User';
            return $response;
        }

        $historyDb = self::with(
            array(
                'destinations' => function ($query) {
                    $query->select(
                        'id','delivery_id','destination_type','invoice_sub_code',
                        'destination_name','destination_address','destination_latitude','destination_longitude',
                        'status','phone_recipient','name_recipient','category','notes','weight','actual_weight','price','pickup_number');
                },
                'transaction' => function ($query){
                    $query->where('transaction_type','delivery');
                },
                'transaction.transactionItems'
            )
        )
            ->select(
                'id','pickup_type','origin_name','origin_address','origin_latitude','origin_longitude',
                'origin_locker_size','item_photo','pickup_date','invoice_code','status','phone_sender','name_sender','notes','total_price','created_at')
            ->where('user_id','=',$userId)
            ->orderBy('created_at','desc')
            // ->first();
            ->paginate(10);

        if ($historyDb->isEmpty()){
            $response->errorMsg = "You don't have any transactions";
            return $response;
        }

        foreach ($historyDb as $item) {
            $item->total_price = (double)$item->transaction->paid_amount;
            $item->promo_amount = (double)$item->transaction->promo_amount;
            $item->original_amount = (double)$item->transaction->total_price;
            $transactionItemData = $item->transaction->transactionItems;
            $destinationData = $item->destinations;

            if (!empty($item->item_photo)){
                $item->item_photo = url($item->item_photo);
            }

            foreach ($destinationData as $destinationDatum) {
                foreach ($transactionItemData as $transactionItemDatum) {
                    $destinationDatum->total_price = $destinationDatum->price;
                    $destinationDatum->promo_amount = 0;
                    $destinationDatum->original_amount = $destinationDatum->price;
                    if ($destinationDatum->id == $transactionItemDatum->item_reference){
                        $destinationDatum->total_price = $transactionItemDatum->paid_amount;
                        $destinationDatum->promo_amount = $transactionItemDatum->promo_amount;
                        $destinationDatum->original_amount = $transactionItemDatum->price;
                    }
                }
            }

            unset($item->transaction);
        }

        $response->isSuccess = true;
        $response->pagination = (int)ceil($historyDb->total()/$historyDb->perPage());
        $response->data = $historyDb->items();

        return $response;
    }

    public function getHistoryDeliveryDetail($userId,$invoiceCode){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $userDb = User::find($userId);
        if (!$userDb){
            $response->errorMsg = 'Invalid Users';
            return $response;
        }

        $leng = strlen($invoiceCode);
        $originType = strtoupper(substr($invoiceCode,-$leng,3));
        if ($originType == 'PLA' || $originType == 'PLL' || $originType == 'PAA' || $originType == 'PAL') {
            // Detail
            $deliveryDb = Delivery::whereHas('destinations',function ($query) use($invoiceCode) {
                $query->where('invoice_sub_code',$invoiceCode);
            })->where('user_id',$userId)
                ->get();
        }else{
            /*Parent*/
            $deliveryDb = Delivery::with('destinations')
                ->where('user_id',$userId)
                ->where('invoice_code',$invoiceCode)
                ->first();
        }

        if (!$deliveryDb){
            $response->errorMsg = 'Invalid Invoice Code for Users';
            return $response;
        }

        $paid_price = 0;
        $promo_price = 0;
        $promo_name = "";
        $promo_description = "";

        // get promo details
        $promoUsed = DB::table('transactions')
            ->select('paid_amount', 'promo_amount', 'id')
            ->where('transaction_id_reference', $deliveryDb->id)
            ->where('transaction_type', 'delivery')
            ->first();

        if (!empty($promoUsed)) {
            $paid_price = (int)$promoUsed->paid_amount;
            $promo_price = (int)$promoUsed->promo_amount;
            $promoDetails = DB::table('campaign_usages')
                ->join('campaigns', 'campaign_usages.campaign_id', '=', 'campaigns.id')
                ->select('campaigns.name', 'campaigns.description')
                ->where('campaign_usages.transaction_id', $promoUsed->id)
                ->first();

            if (!empty($promoDetails)){
                $promo_name = $promoDetails->name;
                $promo_description = $promoDetails->description;
            }
        }

        $deliveryDb->drop_expired_at = $deliveryDb->created_at->addDays(3)->toDateTimeString();

        $deliveryDb->paid_price = $paid_price;
        $deliveryDb->promo_price = $promo_price;
        $deliveryDb->promo_name = $promo_name;
        $deliveryDb->promo_description = $promo_description;

        // get histories
        $deliveryHistory = $deliveryDb->histories;
        foreach ($deliveryHistory as $item) {
            $item['express_date'] = "";
        }

        // get destination
        $deliveryDestination = $deliveryDb->destinations;
        foreach ($deliveryDestination as $item) {
            $destinationHistory = $item->histories;
            $item->histories = $destinationHistory;
        }

        if (!empty($deliveryDb->item_photo)) $deliveryDb->item_photo = url($deliveryDb->item_photo);
        $detailData = $deliveryDb;
        $detailData->destinations = $deliveryDestination;
        $detailData->histories = $deliveryHistory;

        $response->isSuccess = true;
        $response->data = $detailData;

        return $response;
    }

    /**
     * Insert Delivery history
     * @param $deliveryId
     * @param $status
     * @param null $user
     * @param null $remarks
     */
    public function insertDeliveryHistory($deliveryId,$status,$user=null,$remarks=null){
        $historyDb = new DeliveryHistory();
        $historyDb->delivery_id = $deliveryId;
        $historyDb->status = $status;
        $historyDb->user = $user;
        $historyDb->remarks = $remarks;
        $historyDb->save();
    }

    /*====================Relationship====================*/
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function destinations(){
        return $this->hasMany(DeliveryDestination::class,'delivery_id','id');
    }

    public function histories(){
        return $this->hasMany(DeliveryHistory::class,'delivery_id','id');
    }

    public function transaction(){
        return $this->hasOne(Transaction::class,'transaction_id_reference','id');
    }
}
