<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class MalaysiaTariff extends Model
{
    protected $table = 'malaysia_tariff';

    /**
     * Calculate Tariff
     * @param $pickupCity
     * @param $destinationProvince
     * @param $destinationCity
     * @param $destinationDistrict
     * @param int $weight
     * @return \stdClass
     */
    public function calculateTariff($pickupCity,$destinationProvince,$destinationCity,$destinationDistrict,$weight = 1){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->totalPrice = 0;

        $sizeType = 's_price';
        if ($weight <=3) $sizeType = 's_price';
        elseif ($weight <=6) $sizeType = 'm_price';
        elseif ($weight >6) $sizeType = 'l_price';

        // query
        $checkTariff = self::where('origin','LIKE',"%$pickupCity%")
            ->where('destination_province',$destinationProvince)
            ->where('destination_city',$destinationCity)
            // ->where('destination_district',$destinationDistrict)
            ->first();

        if (!$checkTariff){
            $response->errorMsg = 'Tariff Not Found';
            return $response;
        }

        // get price and estimation delivery time
        $price = $checkTariff->{$sizeType};
        $est_delivery = $checkTariff->est_delivery;

        $response->isSuccess = true;
        $response->totalPrice = $price;
        $response->est_delivery = $est_delivery;
        return $response;
    }
}
