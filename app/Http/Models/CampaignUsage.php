<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignUsage extends Model
{
    protected $table = 'campaign_usages';

    /*Relationship*/
    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }
}
