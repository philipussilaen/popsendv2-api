<?php

namespace App\Http\Controllers;

use App\Http\Models\Pages;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function getPage(Request $request, $url=null){
        if (empty($url)){
            abort(404);
        }
        // find URL on DB
        $pagesDb = Pages::where('url',$url)->first();
        if (!$pagesDb){
            abort(404);
        }

        $content = $pagesDb->content;

        // render view
        $data = [];
        $data['content'] = $content;

        return view('pages.render',$data);
    }
}
