<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiHelper;
use App\Http\Library\ApiProx;
use App\Http\Library\Helper;
use App\Http\Models\FreeGift;
use App\Http\Models\FreeGiftCategory;
use App\Http\Models\FreeGiftMerchandise;
use App\Http\Models\UserSession;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Http\Request;

class FreeGiftController extends Controller
{
    /**
     * Post Category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCategory(Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['title', 'icon', 'avaliable_country'];

        //check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput, $request);
        if (!$checkRequired->isSuccess) {
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400, $errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'title' => 'required',
            'icon' => 'required',
            'avaliable_country' => 'required'
        ];
        $categoryModel = new FreeGiftCategory();


        /*cek category code by title*/
        $checkCategory = $categoryModel->checkCategoryCode($request->input('title'));
        if (!$checkCategory->isSuccess) {
            $errorMessage = $checkCategory->errorMsg;
            return ApiHelper::buildResponse(400, $errorMessage);
        }

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ', $errors);
            return ApiHelper::buildResponse(400, $errorMessage);
        }

        /*Insert*/
        $imageUrl = $request->input('icon');
        $categoryDb = $categoryModel->postCategory($request, $imageUrl);
        if (!$categoryDb->isSuccess) {
            $errorMessage = $categoryDb->errorMsg;
            return ApiHelper::buildResponse(400, $errorMessage);
        }

        return ApiHelper::buildResponse(200, null, $categoryDb->data);

    }

    /**
     * get Caategory by Request Country
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategory(Request $request)
    {
        $countryUser = $this->getCountry($request->input('session_id'));
        $sessionId = $request->input('session_id');
        $getUserId = UserSession::getUserBySessionId($sessionId);
        if (!$getUserId->isSuccess){
            $errorMessage = $getUserId->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUserId->userId;

        /*Get Category By Country*/
        $categoryModel = new FreeGiftCategory();
        $categoryDb = $categoryModel->getAllCategory($countryUser);
        if (!$categoryDb->isSuccess) {
            $errorMessage = $categoryDb->errorMsg;
            return ApiHelper::buildResponse(400, $errorMessage);
        }
        $data = $categoryDb->data;

        foreach ($data as $value) {
            unset($value->avaliable_country);
            unset($value->created_at);
            unset($value->updated_at);

            $isAvailable = 1;
            // check if user have use it
            $categoryCode = $value->category_code;
            $freeGift = FreeGift::where('merchandise_code',$categoryCode)->where('user_id',$userId)->first();
            if ($freeGift){
                $isAvailable = 0;
            }

            $value->already_submit = $isAvailable;
        }

        return ApiHelper::buildResponse(200, null, $data);
    }

    /**
     * Get All Question By Category Code
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQuestions(Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['category_code'];

        //check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput, $request);
        if (!$checkRequired->isSuccess) {
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400, $errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'category_code' => 'required'
        ];

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ', $errors);
            return ApiHelper::buildResponse(400, $errorMessage);
        }

        $countryUser = $this->getCountry($request->input('session_id'));
        $categoryModel = new FreeGiftCategory();
        $questions = $categoryModel->getQuestions($request, $countryUser);
        if (!$questions->isSuccess) {
            $errorMessage = $questions->errorMsg;
            return ApiHelper::buildResponse(400, $errorMessage);
        }
        $data = $questions->data;
        unset($data->avaliable_country);
        unset($data->created_at);
        unset($data->updated_at);
        foreach ($data->questions as $datum) {
            $datum->question_code = $datum->id;
            unset($datum->id);
            unset($datum->status);
            unset($datum->avaliable_country);
            unset($datum->created_at);
            unset($datum->updated_at);
            foreach ($datum->choices as $choice) {
                $choice->choice_id = $choice->id;
                unset($choice->id);
                unset($choice->created_at);
                unset($choice->updated_at);
            }
        }
        return ApiHelper::buildResponse(200, null, $data);
    }

    /**
     * Get Merchandise
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMerchandises(Request $request)
    {
        $countryUser = $this->getCountry($request->input('session_id'));
        $merchandises_ = [];
        $merchandises = FreeGiftMerchandise::all();

        if (!$merchandises) {
            return ApiHelper::buildResponse(400, 'No Product Found');
        }


        foreach ($merchandises as $merchandise) {
            foreach (json_decode($merchandise->country) as $country) {
                if ($country == $countryUser) {
                    unset($merchandise->country);
                    unset($merchandise->created_at);
                    unset($merchandise->updated_at);
                    $merchandises_[] = $merchandise;
                }
            }
        }
        return ApiHelper::buildResponse(400, null, $merchandises_);
    }

    /**
     * post answer user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAnswer(Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['occupation', 'birtd_date', 'gender', 'locker_id', 'sample_code'];

        //        check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput, $request);
        if (!$checkRequired->isSuccess) {
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400, $errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'occupation' => 'required',
            'birtd_date' => 'required',
            'gender' => 'required',
            'locker_id' => 'required'
        ];

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ', $errors);
            return ApiHelper::buildResponse(400, $errorMessage);
        }

        $answers = $request->input('categories.*.questions');
        $session_id = $request->input('session_id');
        /*Get User Id*/
        $extraxSession = Helper::getUserId($session_id);
        $user_id = $extraxSession->user_id;
        $freeGiftModel = new FreeGift();
        DB::beginTransaction();
        $freeGiftDb = $freeGiftModel->saveData($request, $answers, $user_id);

        if (!$freeGiftDb->isSuccess) {
            DB::rollback();
            $php_errormsg = $freeGiftDb->errorMesage;
            return ApiHelper::buildResponse(400, $php_errormsg);
        }

        /*Order Prox*/
        $dataFromInsert = $freeGiftDb->data;
        /*Get User Phone*/
        $user = User::find($dataFromInsert->user_id);
        $params = [
            'expressNumber' => $dataFromInsert->invoice_id,
            'takeUserPhoneNumber' => $user->phone,
            'groupName' => 'FREEGIFT'
        ];
        $callProxHelper = new ApiProx();
        $orderProx = $callProxHelper->orderProxFreeGift($params);

        if (!$orderProx->isSuccess) {
            DB::rollback();
            return ApiHelper::buildResponse(200, $orderProx->errorMesage);
        }
        /*Update Order add Prox Id*/
        $freeGiftDb = $freeGiftModel->updateOrder($dataFromInsert->id, $orderProx->data);
        if (!$freeGiftDb->isSuccess) {
            DB::rollback();
            return ApiHelper::buildResponse(200, 'Order Fails');
        }

        DB::commit();

        unset($dataFromInsert->id);
        unset($dataFromInsert->updated_at);
        unset($dataFromInsert->created_at);
        return ApiHelper::buildResponse(200, 'success', $dataFromInsert);
    }

    /**
     * get History
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHistory(Request $request)
    {
        $session_id = $request->input('session_id');
        $pageNumber = $request->input('page',1);

        /*Get User Id*/
        $getUserId = Helper::getUserId($session_id);
        $user_id = $getUserId->user_id;

        $freeGiftModel = new FreeGift();
        $history = $freeGiftModel->getHistory($user_id);
        if (!$history->isSuccess) {
            return ApiHelper::buildResponse(400, $history->errorMesage);
        }
        $data = $history->data;
        $data = collect($data);

        $totalData = count($data);
        // create page list
        $pageList = $data->chunk(10);
        $pages = [];
        foreach ($pageList as $key => $item){
            $pages[] = $key+1;
        }

        // create count and data for current page only
        $chunk = $data->forPage($pageNumber,10);
        $data = $chunk->values()->all();

        return ApiHelper::buildResponsePagination(200,"Success",count($pages),$data);
    }

    /*====================Private Function====================*/
    /**
     * Private function for get country
     * @param $session_id
     * @return mixed
     */
    private function getCountry($session_id)
    {
        /*Get User Id*/
        $getUserBySession = Helper::getUserId($session_id);
        $user_id = $getUserBySession->user_id;

        /*Get Country From User Id*/
        $user = User::find($user_id);
        return $countryUser = $user->country;
    }
}
