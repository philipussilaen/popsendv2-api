<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiHelper;
use App\Http\Library\Helper;
use App\Http\Library\PaymentAPI;
use App\Http\Models\BalanceRecord;
use App\Http\Models\Payment;
use App\Http\Models\PaymentTransaction;
use App\Http\Models\PaymentTransactionHistory;
use App\Http\Models\Promo;
use App\Http\Models\Transaction;
use App\Http\Models\TransactionHistory;
use App\Http\Models\TransactionItem;
use App\Http\Models\UserSession;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Get Available Method
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvailableMethod(Request $request){

        $errorMessage = null;
        $requiredInput = [];
        //check required

        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $country = $request->input('country',null);
        $sessionId = $request->input('session_id');

        if (empty($country)){
            // get user based on session
            $getUser = UserSession::getUserBySessionId($sessionId);
            if (!$getUser->isSuccess){
                $errorMessage = $getUser->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $userId = $getUser->userId;

            $userDb = User::find($userId);
            if (!$userDb){
                $errorMessage = 'Invalid User';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $country = $userDb->country;
        }

        if ($country=='MY'){
            // create param for api payment
            $param = [];
            $param['vendor_code'] = 'MOLPAY';
        }
        else {
            // create param for api payment
            $param = [];
            $param['vendor_code'] = ['BNI','DOKU', 'MANDIRI', 'TCASH'];
        }
        $apiPayment = new PaymentAPI();
        $result = $apiPayment->getAvailableMethod($param);
        if (empty($result)){
            $errorMsg = 'Failed to Get Available Method';
            return ApiHelper::buildResponse(400,$errorMsg);
        }
        if ($result->response->code!=200){
            $errorMsg = $result->response->message;
            return ApiHelper::buildResponse(400,$errorMsg);
        }

        $data = $result->data;
        $availableMethod = [];
        foreach ($data as $item) {
            $availableMethod[] = $item;
        }

        return ApiHelper::buildResponse(200,null,$availableMethod);
    }

    /**
     * Check Payment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkPayment(Request $request){
        $errorMessage = null;
        $requiredInput = ['method_code','amount','session_id'];
        //check required

        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'method_code' => 'required',
            'amount' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // get all variable
        $methodCode = $request->input('method_code');
        $amount = $request->input('amount');
        $sessionId = $request->input('session_id');
        $promoCode = $request->input('promo_code',null);

        // get user based on session
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;

        $userDb = User::find($userId);
        if (!$userDb){
            $errorMessage = 'Invalid User';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $phone = $userDb->phone;
        $country = $userDb->country;

        $response = new \stdClass();
        $response->method_code = '';
        $response->is_promo = false;
        $response->paid_amount = (double)$amount;
        $response->promo_amount = (double)0;
        $response->total_amount = (double)$amount;
        $response->promo_name = null;
        $response->promo_message = null;

        // get member data
        $memberDb = DB::table('users')->where('phone',$phone)->first();
        if (!$memberDb){
            $errorMsg = "Member Phone Not Found";
        }

        $minTopup = 1000;
        if ($country == 'MY') {
            $minTopup = 10;
        }

        if ($amount < $minTopup){
            $errorMsg = "Minimum Top Up for Indonesia is $minTopup";
            if ($country == 'MY')  $errorMsg = "Minimum Top Up for Malaysia is $minTopup";
        }

        $customerName = $memberDb->name;
        $customerPhone = $memberDb->phone;
        $customerEmail = $memberDb->email;

        $params = [];
        $params['transaction_amount'] = $amount;
        $params['transaction_datetime'] = date('Y-m-d H:i:s');
        $params['locker_name'] = null;
        $params['item_amount'] = 1;
        $params['promo_code'] = $promoCode;
        $params['user_phone'] = $customerPhone;
        $params['user_name'] = $customerName;
        $params['user_email'] = $customerEmail;
        $params['country'] = $country;
        $params['service'] = 'popsend-topup';
        $params['payment_method'] = $methodCode;
        $params['topup_method'] = $methodCode;
        $params['topup_amount'] = $amount;

        // check promo
        $checkPromo = Promo::checkPromo($customerPhone,'deposit',$params);
        if ($checkPromo->isSuccess){
            $response->is_promo = true;
            $response->paid_amount = (double)$amount;
            $response->promo_amount = (double)$checkPromo->totalDiscount;
            $response->total_amount = (double)$checkPromo->totalPrice;
            $response->promo_name = $checkPromo->campaignName;
            $response->promo_message = $checkPromo->campaignDescription;
        } else {
            $response->is_promo = false;
            $response->promo_message = $checkPromo->errorMsg;
        }

        return ApiHelper::buildResponse(200,'',$response);

    }

    /**
     * Create Payment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPayment(Request $request){
        $errorMessage = null;
        $requiredInput = ['method_code','amount','session_id'];
        //check required

        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'method_code' => 'required',
            'amount' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $methodCode = $request->input('method_code');
        $amount = $request->input('amount');
        $promoCode = $request->input('promo_code',null);
        $sessionId = $request->input('session_id');

        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;
        $userDb = User::find($userId);
        if (!$userDb){
            $errorMessage = 'Invalid User';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $phone = $userDb->phone;
        $country = $userDb->country;

        // set minimal top up
        $minTopup = 25000;
        if ($country == 'MY'){
            $minTopup = 10;
        }

        // check minimum topup
        if ($amount < $minTopup){
            $errorMessage = "Minimum Top Up for Indonesia is $minTopup";
            if ($country == 'MY')  $errorMessage = "Minimum Top Up for Malaysia is $minTopup";
            return ApiHelper::buildResponsePagination(400,$errorMessage);
        }

        // standard Response
        $response = new \stdClass();
        $response->method_code = $methodCode;
        $response->is_promo = false;
        $response->paid_amount = (double)$amount;
        $response->promo_amount = (double)0;
        $response->total_amount = (double)$amount;
        $response->promo_name = null;
        $response->promo_message = null;

        $response->url = '';
        $response->expired_date = '';
        $response->virtual_number = '';

        $customerId = $userDb->id;
        $customerName = $userDb->name;
        $customerPhone = $userDb->phone;
        $customerEmail = $userDb->email;

        // check promo
        $params = [];
        $params['transaction_amount'] = $amount;
        $params['transaction_datetime'] = date('Y-m-d H:i:s');
        $params['locker_name'] = null;
        $params['item_amount'] = 1;
        $params['promo_code'] = $promoCode;
        $params['user_phone'] = $customerPhone;
        $params['user_name'] = $customerName;
        $params['user_email'] = $customerEmail;
        $params['country'] = $country;
        $params['payment_method'] = $methodCode;
        $params['topup_method'] = $methodCode;
        $params['topup_amount'] = $amount;

        // check promo
        $checkPromo = Promo::checkPromo($customerPhone,'deposit',$params);
        if ($checkPromo->isSuccess){
            $response->is_promo = true;
            $response->paid_amount = (double)$amount;
            $response->promo_amount = (double)$checkPromo->totalDiscount;
            $response->total_amount = (double)$checkPromo->totalPrice;
            $response->promo_name = $checkPromo->campaignName;
            $response->promo_message = $checkPromo->campaignDescription;
        } else {
            $errorMessage = $checkPromo->errorMsg;
            $response->is_promo = false;
            $response->paid_amount = (double)$amount;
            $response->promo_amount = (double)0;
            $response->total_amount = (double)$amount;
            $response->promo_name = null;
            $response->promo_message = $errorMessage;
        }

        DB::beginTransaction();

        $totalPrice = $response->total_amount;
        $paidAmount = $response->paid_amount;
        $promoAmount = $response->promo_amount;

        // create transaction
        $description = "TopUp PopSend $phone,$country,$response->paid_amount";
        $paymentDb = PaymentTransaction::createPayment($userId,$methodCode,$country,$phone,$description,$totalPrice,$paidAmount);
        if (!$paymentDb->isSuccess){
            $errorMessage = "Failed Create Transaction";
            return ApiHelper::buildResponsePagination(400,$errorMessage);
        }
        $paymentId = $paymentDb->paymentId;
        $reference = $paymentDb->reference;

        // create history
        $paymentHistoryDb = PaymentTransactionHistory::createHistory($paymentId,$amount,'CREATED');

        // create transaction
        $transactionModel = new Transaction();
        $createTransaction = $transactionModel->createTopupTransaction($customerId,$paymentId,$reference,$totalPrice,$paidAmount,$promoAmount,$description);
        if (!$createTransaction->isSuccess){
            $errorMessage = $createTransaction->errorMsg;
            return ApiHelper::buildResponsePagination(400,$errorMessage);
        }
        $transactionId = $createTransaction->transactionId;

        // create payment relations
        $paymentModel = new Payment();
        $payment = $paymentModel->savePayment($methodCode, $createTransaction, $paymentDb, $paidAmount);

        $param = [];
        $expiredDate = null;
        // if molpay
        if (strpos($methodCode,'MP') !== false){
            $param['method_code'] = $methodCode;
            $param['amount'] = $amount;
            $param['billing_type'] = 'fixed';
            $param['transaction_id'] = $reference;
            $param['customer_name'] = $customerName;
            $param['customer_email'] = $customerEmail;
            $param['customer_phone'] = $customerPhone;
            $param['description'] = $description;
        }
        elseif (strpos($methodCode,'BNI') !== false || strpos($methodCode,'DOKU') !== false || strpos($methodCode,'TCASH') !== false){
            $param['method_code'] = $methodCode;
            $param['amount'] = $amount;
            $param['billing_type'] = 'fixed';
            $param['transaction_id'] = $reference;
            $param['customer_name'] = $customerName;
            $param['customer_email'] = $customerEmail;
            $param['customer_phone'] = $customerPhone;
            $param['description'] = $description;
            // set va number
            $vaNumber = substr($phone,-10);
            //                    $param['virtual_account'] = $vaNumber;

            // set expired time
            $expiredDate = date('Y-m-d H:i:s',strtotime('+1 days'));
            $param['datetime_expired'] = $expiredDate;
        }

        $apiPayment = new PaymentAPI();
        $result = $apiPayment->createPayment($param);
        if (empty($result)){
            $errorMessage = 'Failed to Get Available Method';
            return ApiHelper::buildResponsePagination(400,$errorMessage);
        }

        if ($result->response->code!=200){
            $errorMessage = $result->response->message;
            return ApiHelper::buildResponsePagination(400,$errorMessage);
        }
        $data = $result->data[0];


        // check promo
        $bookPromo = Promo::bookPromo($customerPhone,'deposit',$transactionId,$expiredDate,$params);
        if ($bookPromo->isSuccess){
            $response->is_promo = true;
            $response->paid_amount = (double)$amount;
            $response->promo_amount = (double)$bookPromo->totalDiscount;
            $response->total_amount = (double)$bookPromo->totalPrice;
            $response->promo_name = $bookPromo->campaignName;
            $response->promo_message = $bookPromo->campaignDescription;
        } else {
            $response->is_promo = false;
            $response->promo_message = $bookPromo->errorMsg;
        }
        if ($response->is_promo){
            $campaignResp = Promo::claimPromo($customerPhone,$transactionId);
            if (!$campaignResp->isSuccess){
                $errorMessage = $campaignResp->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
        }

        // update transaction
        $transactionDb = PaymentTransaction::find($paymentId);
        $transactionDb->data_request = json_encode($param);
        $transactionDb->data_response = json_encode($data);
        $transactionDb->expired_date = $expiredDate;
        $transactionDb->save();

        $response->method_code = $methodCode;
        if (isset($data->url)) $response->url = $data->url;
        if (isset($data->virtual_account_number)) $response->virtual_number = $data->virtual_account_number;
        if (isset($data->datetime_expired)) $response->expired_date = $data->datetime_expired;

        $response->amount = (double)$transactionDb->amount;
        $response->reference = $transactionDb->reference;
        $response->created_date = date('Y-m-d H:i:s',strtotime($transactionDb->created_at));

        DB::commit();
        return ApiHelper::buildResponse(200, null, $response);
    }

    /**
     * Get List Payment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListPayment(Request $request){
        $errorMessage = null;
        $requiredInput = ['session_id'];
        //check required

        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'session_id' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $sessionId = $request->input('session_id');

        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;
        $userDb = User::find($userId);
        if (!$userDb){
            $errorMessage = 'Invalid User';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $phone = $userDb->phone;

        // get List Payment
        $paymentTransactionDb = PaymentTransaction::with('transaction')
            ->where('phone',$phone)
            ->orderBy('id','desc')
            ->paginate(10);

        $listMethod = Cache::get('api-payment-method');
        if (empty($listMethod)){
            // get method list
            $apiPayment = new PaymentAPI();
            $result = $apiPayment->getAvailableMethod();
            $listMethod = [];
            if (!empty($result)){
                if ($result->response->code==200){
                    $listMethod = $result->data;
                    Cache::add('api-payment-method',$listMethod,120);
                }
            }
        }

        $listPayment = [];
        foreach ($paymentTransactionDb as $item) {
            $methodCode = $methodName = $methodText = $methodImage = $methodDescription = $methodLongDescription = $methodStatus = $url = $virtualAccount = $expiredDate = $status = $amount = $promoName = null;
            $promoAmount = 0;
            $paidAmount = 0;
            $totalAmount = 0;

            $methodCode = $item->method;

            // set for URL
            $dataResponse = $item->data_response;
            if (empty($dataResponse)) continue;
            $dataResponse = json_decode($dataResponse);
            if (isset($dataResponse->url)) $url = $dataResponse->url;
            if (isset($dataResponse->virtual_account_number)) $virtualAccount = $dataResponse->virtual_account_number;
            if (isset($dataResponse->datetime_expired)) $expiredDate = $dataResponse->datetime_expired;

            // set for promo
            $transactionDb = $item->transaction()->with(['campaignUsage','campaignUsage.campaign'])->where('transaction_type','topup')->first();
            if ($transactionDb){
                $campaignUsageDb = $transactionDb->campaignUsage;
                $promoAmount = $transactionDb->promo_amount;
                $paidAmount = $transactionDb->paid_amount;
                $totalAmount = $transactionDb->total_price;
                if ($campaignUsageDb){
                    $campaignDb = $campaignUsageDb->campaign;
                    if ($campaignDb){
                        $promoName = $campaignDb->name;
                    }
                }
            }

            $status = $item->status;
            $amount = $item->amount;
            $reference = $item->reference;
            $createdDate = $item->created_at;

            // search on method list
            foreach ($listMethod as $method) {
                if ($method->code == $methodCode){
                    $methodName = $method->name;
                    $methodText = $method->text;
                    $methodImage = $method->image_url;
                    $methodDescription = $method->description;
                    $methodStatus = $method->status;
                    if (!empty($method->long_description)) {
                        $methodLongDescription = $method->long_description;
                    }
                }
            }

            $tmp = new \stdClass();
            $tmp->reference = $reference;
            $tmp->method_code = $methodCode;
            $tmp->method_name = $methodName;
            $tmp->method_text = $methodText;
            $tmp->method_image = $methodImage;
            $tmp->method_status = $methodStatus;
            $tmp->method_description = $methodDescription;
            $tmp->method_long_description = $methodLongDescription;
            $tmp->status = $status;
            $tmp->amount = (float)$amount;
            $tmp->url = $url;
            $tmp->virtual_account_number = $virtualAccount;
            $tmp->datetime_expired = $expiredDate;
            $tmp->promo_amount = (float)$promoAmount;
            $tmp->promo_name = $promoName;
            $tmp->paid_price = (float)$paidAmount;
            $tmp->total_price = (float)$totalAmount;
            $tmp->created_at = date('Y-m-d H:i:s',strtotime($createdDate));
            $listPayment[] = $tmp;
        }

        return ApiHelper::buildResponsePagination(200,'success',$paymentTransactionDb->lastPage(),$listPayment);
    }

    /**
     * Callback Fixed Payment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function callbackFixedPayment(Request $request){
        $response = new \stdClass();
        $response->is_success =false;
        $response->error_msg = null;

        $uniqueId = uniqid();
        Helper::LogPayment("Begin Callback ","payment","callback",$uniqueId);

        $errorMessage = null;
        $requiredInput = ['transaction_id','status'];
        //check required

        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            $message = "Missing Parameter : ".implode(', ',$errorMessage);
            Helper::LogPayment("Callback Failed $message","payment","callback",$uniqueId);
            $response->error_msg = $message;
            return response()->json($response);
        }

        /*Validate value from Input*/
        $rules = [
            'transaction_id' => 'required',
            'status' => 'required',
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            $response->error_msg = $errorMessage;
            return response()->json($response);
        }

        $transactionReference = $request->input('transaction_id');
        $status = $request->input('status');
        $paidAmount = $request->input('paid_amount');

        // find transaction
        $transactionDb = PaymentTransaction::where('reference',$transactionReference)->first();
        if (!$transactionDb){
            $error_msg = 'Invalid Reference';
            Helper::LogPayment("Invalid Transaction Reference","payment","callback",$uniqueId);
            $response->error_msg = $error_msg;
            return response()->json($response);
        }

        DB::beginTransaction();
        $service = 'popsend-topup';
        $phone = $transactionDb->phone;
        $country = $transactionDb->country;
        $errorMsg = null;
        switch ($service){
            case 'popsend-topup':
                // update transaction
                $paymentDb = PaymentTransaction::find($transactionDb->id);
                
                if ($paymentDb->status == 'PAID') {
                    Helper::LogPayment("Already PAID","payment","callback",$uniqueId);
                    $response->is_success = true;
                    return response()->json($response);
                }
                $paymentDb->status = $status;
                $paymentDb->total_amount = $paidAmount;
                $paymentDb->save();
                // topup process
                if ($status=='PAID'){
                    $topUp = $this->topUpPopSend($phone,$paymentDb->id,$transactionReference);
                    if (!$topUp->isSuccess){
                        $errorMessage = "Failed TopUp $topUp->errorMsg";
                        Helper::LogPayment("Failed Top Up ".$topUp->errorMsg,"payment","callback",$uniqueId);
                    }
                }

                // create history
                PaymentTransactionHistory::createHistory($paymentDb->id,$paidAmount,$status);

                // update status payment 
                $paymentRelations = Payment::where('payment_transactions_id', $paymentDb->id)->first();
                $paymentRelations->status = $status;
                $paymentRelations->save();

                break;
            default :
                $errorMsg = 'Invalid Service';
        }

        if (!empty($errorMsg)){
            $error_msg = $errorMsg;
            Helper::LogPayment("Invalid Token","payment","callback",$uniqueId);
            $response->error_msg = $error_msg;
            return response()->json($response);
        }

        DB::commit();
        /*Final Result*/
        Helper::LogPayment("Success","payment","callback",$uniqueId);
        $response->is_success = true;
        return response()->json($response);
    }

    /**
     * Get Available Nominal
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvailableNominal(Request $request){
        $userCountry = $this->getCountry($request->input('session_id'));
        if ($userCountry == 'MY') {
            $arrAmount = array(
                // array("name"=> "10", "type" => "web|mobile", "price"=> "10", "discount" => "0%", "nominal" => 0),
                array("name"=> "20", "type" => "web|mobile|terminal", "price"=> "20", "discount" => "0%", "nominal" => 20),
                array("name"=> "50", "type" => "web|mobile|terminal", "price"=>"50", "discount" => "0%", "nominal" => 50),
                array("name"=> "100", "type" => "web|mobile|terminal", "price"=>"100", "discount" => "0%", "nominal" => 100));
        } else {
            $arrAmount = array(
                array("name"=> "25k", "type" => "web|mobile|terminal", "price"=> "25000", "discount" => "0%", "nominal" => 25000),
                array("name"=> "50k", "type" => "web|mobile|terminal", "price"=>"50000", "discount" => "0%", "nominal" => 50000),
                array("name"=> "100k", "type" => "web|mobile|terminal", "price"=>"100000", "discount" => "0%", "nominal" => 100000),
                array("name"=> "200k", "type" => "web|mobile|terminal", "price"=>"200000", "discount" => "0%", "nominal" => 200000),
                array("name"=> "500k", "type" => "web|mobile", "price"=>"500000", "discount" => "0%", "nominal" => 500000));
        }

        return ApiHelper::buildResponse(200,null,$arrAmount);
    }

    /*======================= PRIVATE FUNCTION =======================*/
    /**
     * private function for get country
     * @param $session_id
     * @return mixed
     */
    private function getCountry ($session_id)
    {
        /*Get User Id*/
        $getSessionDb = Helper::getUserId($session_id);
        $user_id = $getSessionDb->user_id;

        /*Get Country From User Id*/
        $user = User::find($user_id);
        return $countryUser = $user->country;
    }
    /**
     * topup Send
     * @param $phone
     * @param $id_transaction
     * @param $referralTransactionId
     * @return \stdClass
     */
    private function topUpPopSend($phone,$id_transaction,$referralTransactionId){
        $response = new \stdClass();
        $response->isSuccess = true;
        $response->errorMsg = null;

        // check member
        $memberDb = User::where('phone',$phone)
            ->first();
        if (!$memberDb){
            $response->errorMsg = "Member Phone Not Found";
            return $response;
        }
        $userId = $memberDb->id;

        /*Find Transaction*/
        $transactionModel = Transaction::where('transaction_type','topup')->where('transaction_id_reference',$id_transaction)->first();
        if (!$transactionModel) {
            $response->isSuccess = false;
            $response->errorMsg = "Transaction Not Found";
            return $response;
        }
        $transactionId = $transactionModel->id;
        $topUpAmount = $transactionModel->total_price;

        $transactionDb = Transaction::find($transactionId);
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // find transaction item
        $transactionItemDb = TransactionItem::where('transaction_id',$transactionId)->get();
        foreach ($transactionItemDb as $item) {
            $itemDb = TransactionItem::find($item->id);
            $itemDb->status = 'PAID';
            $itemDb->save();
        }

        /*Insert TO Balance Record*/
        $balanceModel = new BalanceRecord();
        $insertBalance = $balanceModel->creditDeposit($userId,$topUpAmount,$transactionId);
        if (!$insertBalance->isSuccess) {
            $response->isSuccess = false;
            $response->errorMsg = $insertBalance->errorMsg;
            return $response;
        }

        /*Update Balance In Users Table*/
        $memberDb->balance = $memberDb->balance + $topUpAmount;
        $memberDb->save();

        $historyModel = new TransactionHistory();
        $userName = $memberDb->name;
        $description = 'Paid TopUp';
        $insertHistory = $historyModel->insertHistory($transactionId,$userName,$description,$transactionDb->total_price,$transactionDb->paid_amount,$transactionDb->promo_amount,0,'PAID','System');

        $response->isSuccess = true;
        return $response;
    }
}
