<?php

namespace App\Http\Controllers;
use App\Http\Models\CategoryList;
use App\Http\Models\GeneralSetting;
use App\Http\Models\Promo;
use App\Http\Models\TransactionHistory;
use App\Http\Models\UserDevice;
use App\Http\Models\UserSession;
use App\Http\Models\Users;
use App\Http\Models\ApiToken;
use Illuminate\Support\Carbon;
use App\Http\Library\ApiProx;
use App\Http\Models\LockerLocation;
use App\Http\Models\LockerAvailabilityReport;
use CodeItNow\BarcodeBundle\Utils\QrCode;
use App\Http\Library\CurlHelper;
use App\Http\Library\Helper;
use App\Http\Models\BalanceRecord;
use App\Http\Models\Popsafe;
use App\Http\Models\Transaction;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Library\ApiHelper;
use Illuminate\Http\Request;
use App\Http\Models\PopSafeCSR;
use App\Http\Models\PopSafeLocker;
use App\Http\Models\PaymentMethod;
use App\Http\Models\Payment;
use App\Http\Models\Companies;

class OrderPopsafeController extends Controller
{
    /**
     * Calculate PopSafe
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    protected $forceAllowedLockers = [
    '4910adbe08c304723ebccf2dcf6916db', // JFK 2018 Hall E Foodcourt
    '07f2096b212c5b772921bf8cef158eed' // JFK 2018 Hall B3
    ];

    protected $forceClose = false; // Bypass disabling popsafe

    public function popSafeCalculate (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['locker_id','size'];
        $sessionId = $request->input('session_id');
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;
        $userDb = User::find($userId);
        if (!$userDb){
            $errorMessage = "Invalid User";
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $country = $userDb->country;
        if (!in_array($request->input('locker_id'), $this->forceAllowedLockers)){
            if($country == 'ID') {
                if ($this->forceClose){
                    $errorMessage = "We are sorry, this service is temporarily unavailable due to maintenance schedule. Please check our social media for further notice.";
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
            }
        }

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'locker_id' => 'required',
            'size' => 'required|in:S,M,L,XL'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $sessionId = $request->input('session_id');
        $lockerId = $request->input('locker_id');
        $lockerSize = $request->input('size');
        $promoCode = $request->input('promo_code',null);

        // get session ID
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;
        $userDb = User::find($userId);
        if (!$userDb){
            $errorMessage = "Invalid User";
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $country = $userDb->country;
        $phone = $userDb->phone;

        // set base price
        $basePrice = 6000;
        if ($country == 'MY') {
            $basePrice = 3;
        }
        // get from general setting
        if ($country == 'ID'){
            $generalSetting = GeneralSetting::where('key','popsend-price-id')->first();
            if ($generalSetting){
                $basePrice = (float)$generalSetting->value;
            }
        } elseif ($country == 'MY'){
            $generalSetting = GeneralSetting::where('key','popsend-price-my')->first();
            if ($generalSetting){
                $basePrice = (float)$generalSetting->value;
            }
        }

        // get locker
        $lockerModel = new LockerLocation();
        $lockerDb = $lockerModel->getLockerByLockerId($lockerId);
        if (empty($lockerDb)){
            $errorMessage = 'Invalid Locker';
            return ApiHelper::buildResponse(400,$errorMessage);
        } else if (!empty($lockerDb)) {
            // Check if Locker is Online
            if ($lockerDb->online_status == false) {
                $errorMessage = "Locker @ $lockerDb->name is currently OFFLINE, please try again later or choose another locker";
                return ApiHelper::buildResponse(400,$errorMessage);
            }
        }
        
        $lockerName = $lockerDb->name;

        $params = [];
        $params['transaction_amount'] = $basePrice;
        $params['transaction_datetime'] = date('Y-m-d H:i:s');
        $params['locker_name'] = $lockerName;
        $params['locker_size'] = $lockerSize;
        $params['item_amount'] = 1;
        $params['promo_code'] = $promoCode;
        $params['user_phone'] = $phone;
        $params['transaction_type'] = 'popsafe';

        $totalPrice = $basePrice;
        $promoName = null;
        $promoDescription = null;
        $promoAmount = 0;

        $campaignResp = Promo::checkPromo($phone,'potongan',$params);
        $campaignName = null;
        $discAmount = null;
        if ($campaignResp->isSuccess) {
            $totalPrice = $campaignResp->totalPrice;
            $promoName = $campaignResp->campaignName;
            $promoAmount = $campaignResp->totalDiscount;
            $promoDescription = $campaignResp->campaignDescription;
        }

        // create response
        $tmp = new \stdClass();
        $tmp->price = $basePrice;
        $tmp->paid_price = $totalPrice;
        $tmp->promo_amount = $promoAmount;
        $tmp->promo_name = $promoName;
        $tmp->promo_description = $promoDescription;
        $tmp->locker_name = $lockerName;
        $tmp->locker_size = $lockerSize;

        return ApiHelper::buildResponse(200,'Success Calculate',$tmp);
    }

    /**
     * Post Oder Popsafe
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function postOrder (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['locker_id','locker_size','notes','item_photo'];

        $sessionId = $request->input('session_id');
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;
        $userDb = User::find($userId);
        if (!$userDb){
            $errorMessage = "Invalid User";
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $country = $userDb->country;
        if (!in_array($request->input('locker_id'), $this->forceAllowedLockers)){
            if($country == 'ID') {
                if ($this->forceClose){
                    $errorMessage = "We are sorry, this service is temporarily unavailable due to maintenance schedule. Please check our social media for further notice.";
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
            }
        }

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'locker_id' => 'required',
            'locker_size' => 'required|in:S,M,L,XL',
            'auto_extend' => 'nullable|in:0,1',
            'category_id' => 'nullable',
            'item_description' => 'nullable'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $sessionId = $request->input('session_id');
        $lockerId = $request->input('locker_id');
        $lockerSize = $request->input('locker_size');
        $notes = $request->input('notes');
        $itemPhoto = $request->input('item_photo');
        $autoExtend = $request->input('auto_extend',0);
        $promoCode = $request->input('promo_code',null);
        $categoryId = $request->input('category_id',null);
        $itemDescription = $request->input('item_description',null);
        
        $request->request->add(['size' => $lockerSize]);

        $calculateOrder = $this->popSafeCalculate($request)->getOriginalContent();
        if ($calculateOrder->response->code!=200){
            $errorMessage = $calculateOrder->response->message;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $calculateData = $calculateOrder->data[0];
        $basePrice = $calculateData->price;
        $promoAmount = $calculateData->promo_amount;

        // get user ID based on Session
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;
        $userDb = User::find($userId);
        if (!$userDb){
            $errorMessage = 'Invalid User';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userPhone = $userDb->phone;
        $userName = $userDb->name;
        $userEmail = $userDb->email;
        $country = $userDb->country;

        // get locker data
        $lockerModel = new LockerLocation();
        $lockerDb = $lockerModel->getLockerByLockerId($lockerId);
        if (empty($lockerDb)){
            $errorMessage = 'Invalid Locker';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        DB::beginTransaction();

        // create order
        $popsafeModel = new Popsafe();
        $createOrder = $popsafeModel->createOrder($userId,$lockerId,$lockerSize,$basePrice,$autoExtend,$notes,$itemPhoto,$categoryId,$itemDescription);
        if (!$createOrder->isSuccess){
            $errorMessage = $createOrder->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $popSafeId = $createOrder->popSafeId;
        $pathPhoto = $createOrder->pathPhoto;
        $popSafeDb = $popsafeModel->find($popSafeId);
        $popSafeInvoice = $popSafeDb->invoice_code;
        $popSafeData = $createOrder->data;

        // create transaction
        $transactionModel = new Transaction();
        $createTransaction = $transactionModel->createPopsafeTransaction($userId,$popSafeId,$promoAmount);
        if (!$createTransaction->isSuccess){
            $errorMessage = $createTransaction->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $transactionId = $createTransaction->transactionId;

        $transactionDb = $transactionModel->find($transactionId);
        $paidAmount = $transactionDb->paid_amount;

        // debit transaction
        $balanceModel = new BalanceRecord();
        $debitBalance =$balanceModel->debitDeposit($userId,$paidAmount,$transactionId);
        if (!$debitBalance->isSuccess){
            DB::rollback();
            $errorMessage = $debitBalance->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // if promo code exist
        $promoName = null;
        $promoDescription = null;
        if ($promoCode){
            $params = [];
            $params['transaction_amount'] = $basePrice;
            $params['transaction_datetime'] = date('Y-m-d H:i:s');
            $params['locker_name'] = $calculateData->locker_name;
            $params['locker_size'] = $lockerSize;
            $params['item_amount'] = 1;
            $params['promo_code'] = $promoCode;
            $params['user_phone'] = $userPhone;
            $params['transaction_type'] = 'popsafe';
            $bypassPromo = false;

            $campaignResp = Promo::bookPromo($userPhone,'potongan',$transactionId,'',$params);
            if (!$campaignResp->isSuccess){
//                $errorMessage = $campaignResp->errorMsg;
//                return ApiHelper::buildResponse(400,$errorMessage);
                $bypassPromo = true;
            }
            if (!$bypassPromo){
                $promoName = $campaignResp->campaignName;
                $promoDescription = $campaignResp->campaignDescription;
                $campaignResp = Promo::claimPromo($userPhone,$transactionId);
                if (!$campaignResp->isSuccess){
                    $errorMessage = $campaignResp->errorMsg;
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
            }
        }

        // push to prox
        $apiProx = new ApiProx();
        $dataProx = [
            'userPhone' => $userPhone,
            'userName'  => $userName,
            'invoiceCode' => $popSafeInvoice,
            'address' => $popSafeDb->locker_name
        ];
        $orderProx = $apiProx->importPopSafe($dataProx);
        if (!$orderProx->isSuccess){
            $errorMessage = 'Failed To Push Locker';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $proxData = $orderProx->data;
        $expressId = $proxData->id;
        $popSafeDb = Popsafe::find($popSafeId);
        $popSafeDb->express_id = $expressId;
        $popSafeDb->save();

        if ($pathPhoto){
            $structure = 'media_/popsafe_/'.date('Y-M');
            if(!is_dir($structure)){
                mkdir($structure, 0777, true);
            }
            $decode = base64_decode($itemPhoto);
            file_put_contents($pathPhoto,$decode);
        }
        DB::commit();

        /*Send Email And SMS*/
        $qrCode = new QrCode();
        $qrCode
            ->setText($popSafeInvoice)
            ->setSize(200)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
            ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
            ->setLabel('Scan Qr Code')
            ->setLabelFontSize(13)
            ->setImageType(QrCode::IMAGE_TYPE_PNG);

        $popSafeData['email'] = $userEmail;
        $popSafeData['name'] = $userName;
        $popSafeData['QRcode'] = $qrCode;
        $popSafeData['amount'] = (float)$transactionDb->total_price;
        $popSafeData['paid_amount'] = (float)$transactionDb->paid_amount;
        $popSafeData['promo_amount'] = (float)$transactionDb->promo_amount;
        $popSafeData['promo_name'] = $promoName;
        $popSafeData['promo_description'] = $promoDescription;

        //generate PDF
        $output = PDF::loadView('pdf.invoice', $popSafeData);

        // send email
       Mail::send('email.popsafe.ordercreated',$popSafeData, function($message) use ($popSafeData,$output){
            $message->subject('PopSafe Invoice #'.$popSafeData['invoice_id']);
            $message->to( $popSafeData['email'],$popSafeData['name']);
            $message->from('no-reply@popbox.asia','PopSend');
            $message->attachData($output->output(),$popSafeData['invoice_id'].'.pdf');
        });

        if ($country == 'ID'){
            $smsMessage = "Hi $userName, Order PopSafe Anda Sukses. Gunakan Kode: $popSafeInvoice Untuk Membuka Locker Anda";
        } else {
            $smsMessage = "Hi $userName, You Have Successfully Order PopSafe. Use this Code : $popSafeInvoice to Open the Locker";
        }

        // send SMS
        $sendSms = Helper::sendSms($userPhone,$smsMessage);

        //remove QRcode for return
        unset($popSafeData['QRcode']);
        return ApiHelper::buildResponse(200,'Order Success',[$popSafeData]);
    }

    /**
     * Update Status Parcel from locker
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function callbackLocker(Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['invoice_id','status','locker_number','code_pin','remarks','parcel_id'];
        Helper::LogPayment($request->input('invoice_id') ." - ".$request->getContent(), 'callbackLocker', 'locker');
        //check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        
        /*Validate value from Input*/
        $rules = [
            'invoice_id' => 'required',
            'status' => 'required|in:IN_STORE,CANCEL,CUSTOMER_TAKEN,OPERATOR_TAKEN',
            'locker_number' => 'required',
            'code_pin' => 'required',
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $invoiceId = $request->input('invoice_id');
        $status = $request->input('status');
        $lockerNumber = $request->input('locker_number');
        $codePin = $request->input('code_pin');
        $remarks = $request->input('remarks');
        $parcelId = $request->input('parcel_id');
        $dataOperator = $request->input('dataOperator', null);
        $storelocker_time = $request->input('locker_time', null);
        $locker_time = null;
        
        if ($storelocker_time != null) {
            $locker_time = date('Y-m-d H:i:s', substr($storelocker_time, 0, 10));
        }

        if ($status == 'CUSTOMER_TAKEN' || $status == 'OPERATOR_TAKEN') {
            $status = "COMPLETE|".$status;
        } elseif ($status == "IN_STORE") {
            $status = "IN STORE";
        }else{
            $status = "CANCEL";
        }
        
        $checkPopsafe = Popsafe::where('invoice_code', $invoiceId)->first();
        if ($checkPopsafe == null) {
            $data[] = [
                'popsafe' => 'Data Not Found'
            ];
        } elseif ($checkPopsafe->status != 'COMPLETE') {
            DB::beginTransaction();
    
            if (substr($invoiceId, 0, 5) == 'APXID') {
                $dataRequest = json_decode($request->getContent());
                $dataExtend = json_decode($dataRequest->request);
                $locker_time = date('Y-m-d H:i:s', substr($dataExtend->takeTime, 0, 10));
                $checkProperty = property_exists((object)$dataExtend, 'isExtend');
                if ($checkProperty) {
                    if ($dataExtend->isExtend) {
                        $extend = $this->extendPopSafeLocker($dataExtend, $request->input('token'));
                    }
                }
            }
    
            $popsafeModel = new Popsafe();
            $updatePopsafeDb = $popsafeModel->updatePopsafeStatus($invoiceId,$status,$remarks,$lockerNumber,$codePin,$dataOperator,$locker_time);
    
            if (!$updatePopsafeDb->isSuccess){
                $errorMessage = $updatePopsafeDb->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
    
            DB::commit();
    
            // update overdue
            $popSafeDb = Popsafe::where('invoice_code',$invoiceId)->first();
    
            $updateData = Popsafe::find($popSafeDb->id);
            $updateData->parcel_id = $parcelId;
            $updateData->save();
    
            $expiredDateTime = $popSafeDb->expired_time;
    
            $overdueDate = date('Y-m-d H:i:s',strtotime($expiredDateTime));
            $unixOverdueDateTime = strtotime($overdueDate) * 1000;
    
            // post to prox to change overdue if status == in store
            if ($status == 'IN STORE'){
                $apiProx = new ApiProx();
                $changeOverdue = $apiProx->changeOverdueDateTime($parcelId,$unixOverdueDateTime);
            }
    
            $data[] = [
                'status' => $updatePopsafeDb->status
            ];
        } else {
            $status = 'ALREADY COMPLETE';
            $data[] = [
                'status' => $status
            ];
        }

        return ApiHelper::buildResponse(200,null,$data);
    }

    /**
     * Manual Extend PopSafe
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function extendPopSafe(Request $request){
        $errorMessage = null;
        $requiredInput = ['invoice_id','session_id'];
        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'invoice_id' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $invoiceCode = $request->input('invoice_id');
        $sessionId = $request->input('session_id');

        // This param only be sent from lockers
        $getRemarks = $request->input('remarks');
        
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;

        DB::beginTransaction();

        // extend expired time of popsafes
        $popSafeModel = new Popsafe();
        $extend = $popSafeModel->extendPopSafe($invoiceCode);
        if (!$extend->isSuccess){
            DB::rollback();
            $errorMessage = $extend->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $popSafeId = $extend->popSafeId;

        // get popsafesDB
        $popSafeDb = Popsafe::find($popSafeId);
        $totalPrice = $popSafeDb->total_price;
        $parcelId = $popSafeDb->parcel_id;
        $expiredDateTime = $popSafeDb->expired_time;

        if (empty($parcelId)){
            DB::rollback();
            $errorMessage = 'Failed to Extend. Empty Parcel';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // create new transaction
        $transactionModel = new Transaction();
        $remarks = "Extend PopSafe $invoiceCode";
        if (!empty($getRemarks)) $remarks = $getRemarks;
        $createTransaction = $transactionModel->createPopsafeTransaction($userId,$popSafeId,0,$remarks);
        if (!$createTransaction->isSuccess){
            DB::rollback();
            $errorMessage = $createTransaction->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $transactionId = $createTransaction->transactionId;
        $transactionDb = $transactionModel->find($transactionId);
        $paidAmount = $transactionDb->paid_amount;

        if (empty($getRemarks)){
            // debit transaction
            $balanceModel = new BalanceRecord();
            $debitBalance =$balanceModel->debitDeposit($userId,$paidAmount,$transactionId);
            if (!$debitBalance->isSuccess){
                DB::rollback();
                $errorMessage = $debitBalance->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }

            // change overdue to prox
            $overdueDate = date('Y-m-d H:i:s',strtotime($expiredDateTime));
            $unixOverdueDateTime = strtotime($overdueDate) * 1000;
            $apiProx = new ApiProx();
            $changeOverdue = $apiProx->changeOverdueDateTime($parcelId,$unixOverdueDateTime);
            if (empty($changeOverdue)){
                DB::rollback();
                $errorMessage = 'Failed to Extend on locker';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            if ($changeOverdue->response->code!=200){
                DB::rollback();
                $errorMessage = $changeOverdue->response->message;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
        }

        DB::commit();

        // get popsafe data
        $popSafeData = Popsafe::getList($userId,$invoiceCode);
        $data = $popSafeData->data;

        // send push notification FCM
//        $userDevices = UserDevice::where('user_id', $userId)->orderBy('created_at', 'desc')->first();
//        $sendNotif = Helper::sendPushNotificationFCM($userDevices->gcm_token, 'PopSafe', 'Extend Success for Order PopSafe '.$invoiceCode.', Expired '.$data->expired_time.'', $invoiceCode, 'popsafe');

        return ApiHelper::buildResponse(200,null,$data);
    }


    public function extendPopSafeLocker($request, $token)
    {
        $errorMessage = null;
        $minutes = 0;
        $take_time_locker = null;
        $extend_times = null;
        $apiToken = new ApiToken();
        $checkClientExist = $apiToken->checkClientByToken($token);   
        
        if (!$checkClientExist->isSuccess){
            $errorMessage = $checkClientExist->errMessage;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        if (property_exists((object)$request, 'minutes_duration')) {
            $minutes = $request->minutes_duration;
        }

        if (property_exists((object)$request, 'timestamp_duration')) {
            $take_time_locker = (int)$request->timestamp_duration;
        }

        $invoiceCode = $request->expressNumber;
        $extendTimes = $request->extendTimes;
        $duration = $request->duration;
        $expressId = $request->id;
        $pinCode = $request->validateCode;
        $storeTime = $request->storeTime;
        $extendDate = $request->overdueTime;
        $lockerId = $request->box_id;
        $paymentMethod = $request->paymentMethod;
        $paymentAmount = $request->cost_overdue;
        $phoneNo = $request->takeUserPhoneNumber;
        $transactionRecord = $request->transactionRecord;

        // $notes = $request->input('transactionRecord', 'PopSafe Locker - '.$invoiceCode);
        // $itemDescription = $paymentMethod .' | '.$request->input('transactionRecord');
        
        // get client id companies 
        $clienCompanies = new Companies();
        $companies = $clienCompanies->checkClientAvailable($invoiceCode);
        
        if (!$companies->isSuccess) {
            $errorMessage = $companies->message;
            return ApiHelper::buildResponse(400,$errorMessage);            
        }
        $companies = $companies->data;


        // Get User
        $getUser = Users::getSessionIdByPhone($phoneNo);
        $userId = $getUser->userId;
        $userDb = User::find($userId);

        // get method Payment
        $paymentMethodModel = new PaymentMethod();
        $paymentMethodDb = $paymentMethodModel->getMethod($paymentMethod);
        
        if (!$paymentMethodDb->isSuccess) {
            $errorMessage = $paymentMethodDb->message;
            return ApiHelper::buildResponse(400,$errorMessage);            
        }
        $paymentMethodId = $paymentMethodDb->data->id;
        $paymentName = $paymentMethodDb->data->name;

        // BEGIN TRANSACTIONAL
        DB::beginTransaction();

        $popsafeDb = PopsafeLocker::where('invoice_code', $invoiceCode)->first();
        $popSafeId = $popsafeDb->id;
            
        // get popsafesDB
        $popSafeDb = Popsafe::find($popSafeId);
        $popSafeDb->minutes_duration = $minutes;
        $popSafeDb->take_time_locker = $take_time_locker;
        $popSafeDb->extend_times = $extendTimes;
        $totalPrice = $paymentAmount;
        $parcelId = $popSafeDb->parcel_id;
        $expiredDateTime = $popSafeDb->expired_time;
        $popSafeDb->save();

        if (empty($parcelId)){
            DB::rollback();
            $errorMessage = 'Failed to Extend. Empty Parcel';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // create new transaction
        $remarks = "Extend PopSafe $invoiceCode duration $duration hours $minutes minutes with $paymentName | $transactionRecord";
       
        // if (!empty($getRemarks)) $remarks = $getRemarks;
        $transactionModel = new Transaction();
        $createTransaction = $transactionModel->createPopsafeLockerTransaction($invoiceCode, $userId,$popSafeId,0,$remarks, $paymentMethodId, $companies, $paymentAmount);
        if (!$createTransaction->isSuccess){
            DB::rollback();
            $errorMessage = $createTransaction->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // Creaet Payment
        $paymentModel = new Payment();
        $paymentTransaction = $paymentModel->createPaymentExtend($request, $paymentMethodId, $createTransaction, $userDb, $remarks);

        if (!$paymentTransaction->isSuccess){
            $errorMessage = "Failed To Create Payment Transaction";
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $transactionId = $createTransaction->transactionId;
        $transactionDb = $transactionModel->find($transactionId);
        $paidAmount = $paymentAmount;

        DB::commit();

        $popSafeData = Popsafe::getList($userId,$invoiceCode);
        $data = $popSafeData->data;
        return ApiHelper::buildResponse(200,null,$data);
    }

    /**
     * Cancel Order
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelOrder (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['invoice_id','remarks','session_id'];
        //check required

        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'invoice_id' => 'required',
            'remarks' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $invoiceId = $request->input('invoice_id');
        $remarks = $request->input('remarks');
        $sessionId = $request->input('session_id');

        // get user ID based on Session
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;

        //check time cancel 1 hour after order
        $popsafeModel = new Popsafe();
        $popsafeDb = $popsafeModel->where('invoice_code','=',$invoiceId)->first();
        if (!$popsafeDb) {
            $errorMessage = 'Invoice Id Not Found';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $expressId = $popsafeDb->express_id;

        $dateTimeNow = time();
        if($dateTimeNow >= strtotime($popsafeDb->cancellation_time)){
            $errorMessage = 'Time Cancel Expired';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        if ($popsafeDb->status == 'CANCEL'){
            $errorMessage = "PopSafe Already Cancelled";
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        DB::beginTransaction();

        //update cancel status and History
        $popsafeUpdateDb = $popsafeModel->updatePopsafeStatus($invoiceId,'CANCEL',$remarks);
        if (!$popsafeUpdateDb->isSuccess){
            DB::rollback();
            $errorMessage = $popsafeUpdateDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // remove from prox
        $apiProx = new ApiProx();
        $deleteImported = $apiProx->deleteImport($expressId);
        if (empty($deleteImported)){
            DB::rollback();
            $errorMessage = 'Failed Cancel on locker';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        if ($deleteImported->response->code != 200){
            $errorMessage = 'Failed Cancel on Locker';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        DB::commit();

        // get popsafe
        $getDetail = Popsafe::getList($userId,$invoiceId);
        $data = $getDetail->data;

        if(!empty($data)) {
            $this->refundCancelOrder($invoiceId, $remarks, $sessionId);
            return ApiHelper::buildResponse(200,null,$data);
        } else {
            return ApiHelper::buildResponse(400,'Failed Cancel on Locker');
        }

    }

    /**
     * Refund Order
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function refundOrder (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['invoice_id','remarks','session_id'];
        //check required

        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'invoice_id' => 'required',
            'remarks' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $invoiceId = $request->input('invoice_id');
        $remarks = $request->input('remarks');
        $sessionId = $request->input('session_id');

        // get user ID based on Session
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;

        // get transaction
        $transactionDb = TransactionHistory::join('transactions', 'transactions.id', '=', 'transaction_histories.transaction_id')
            ->whereRaw("transaction_histories.description LIKE '%".$invoiceId."%'")
            ->select('transactions.*')
            ->first();
        if (!$transactionDb){
            return ApiHelper::buildResponse(400,'Invalid Transaction Invoice ID');
        }
        $transactionId = $transactionDb->id;

        $allowedRefund = ['popsafe'];

        // get transaction type
        $transactionType = $transactionDb->transaction_type;
        $transactionStatus = $transactionDb->status;
        if (!in_array($transactionType,$allowedRefund)){
            return ApiHelper::buildResponse(400,'Transaction Not Allowed to Refund');
        }
        if ($transactionStatus!='PAID'){
            return ApiHelper::buildResponse(400,'Transaction Not PAID. Status: '.$transactionStatus);
        }

        DB::beginTransaction();
        if ($transactionType == 'popsafe'){
            $transactionModel = new Transaction();
            $refundTransaction = $transactionModel->refundTransaction($transactionId,$remarks);
            if (!$refundTransaction->isSuccess){
                DB::rollback();
                return ApiHelper::buildResponse(400,$refundTransaction->errorMsg);
            }

            DB::commit();
            // get popsafe
            $getDetail = Popsafe::getList($userId,$invoiceId);
            $data = $getDetail->data;
            return ApiHelper::buildResponse(200,null,$data);
        }
        else {
            DB::rollback();
            return ApiHelper::buildResponse(400,'Invalid Type');
        }

    }

    /**
     * Refund Cancel Order
     * @param Request $invoiceId
     * @param Request $remarks
     * @param Request $sessionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function refundCancelOrder ($invoiceId, $remarks='', $sessionId)
    {
        // get user ID based on Session
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUser->userId;

        // get transaction
        $transactionDb = TransactionHistory::join('transactions', 'transactions.id', '=', 'transaction_histories.transaction_id')
            ->whereRaw("transaction_histories.description LIKE '%".$invoiceId."%'")
            ->select('transactions.*')
            ->first();
        if (!$transactionDb){
            return ApiHelper::buildResponse(400,'Invalid Transaction Invoice ID');
        }
        $transactionId = $transactionDb->id;

        $allowedRefund = ['popsafe'];

        // get transaction type
        $transactionType = $transactionDb->transaction_type;
        $transactionStatus = $transactionDb->status;
        if (!in_array($transactionType,$allowedRefund)){
            return ApiHelper::buildResponse(400,'Transaction Not Allowed to Refund');
        }
        if ($transactionStatus!='PAID'){
            return ApiHelper::buildResponse(400,'Transaction Not PAID. Status: '.$transactionStatus);
        }

        DB::beginTransaction();
        if ($transactionType == 'popsafe'){
            $transactionModel = new Transaction();
            $refundTransaction = $transactionModel->refundTransaction($transactionId,$remarks);
            if (!$refundTransaction->isSuccess){
                DB::rollback();
                return ApiHelper::buildResponse(400,$refundTransaction->errorMsg);
            }

            DB::commit();
            // get popsafe
            $getDetail = Popsafe::getList($userId,$invoiceId);
            $data = $getDetail->data;
            return ApiHelper::buildResponse(200,null,$data);
        }
        else {
            DB::rollback();
            return ApiHelper::buildResponse(400,'Invalid Type');
        }

    }

    /**
     * Get Popsafe History
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function popSafeHistory (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['page'];

        //check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'page' => 'required|integer'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $user_id = Helper::getUserId($request->input('session_id'));
        $safeHistoryDb = Popsafe::getList($user_id->user_id);

        if (!$safeHistoryDb->isSuccess) {
            $errorMessage = $safeHistoryDb->errorMsg;
            return ApiHelper::buildResponsePagination(400,$errorMessage);
        }

        return ApiHelper::buildResponsePagination(200,null,$safeHistoryDb->pagination,$safeHistoryDb->data);
    }

    /**
     * Get History Detail
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function popSafeHistoryDetail(Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['invoice_id'];
        //check required

        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'invoice_id' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $invoiceCode = $request->input('invoice_id');
        $popsafeModel = new Popsafe();
        $historyDb = $popsafeModel->listDetail($invoiceCode);
        if (!$historyDb->isSuccess) {
            $errorMessage = $historyDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $data= $historyDb->data;
        return ApiHelper::buildResponse(200,null,$data);
    }

    /**
     * Get Category List
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategoryList(Request $request){
        $errorMessage = null;
        $requiredInput = [];
        //check required

        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'type' => 'nullable|in:popsafe,popsend'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $type = $request->input('type',null);

        // get category List
        $categoryList = CategoryList::when($type,function ($query) use ($type){
            if ($type == 'popsend')  $query->where('popsend','1');
            elseif ($type == 'popsafe') $query->where('popsafe','1');
        })
            ->select('id','name','parent_id')
            ->orderBy('name', 'asc')
            ->get()
            ->toArray();

        if (empty($categoryList)){
            $errorMessage = 'Empty Category';
            return ApiHelper::buildResponse(200,$errorMessage);
        }

        $tree = $this->categoryTree($categoryList);

        return ApiHelper::buildResponse(200,null,$tree);
    }

    /**
     * Private - Build Tree
     * @param array $elements
     * @param int $parentId
     * @return array
     */
    private function buildTree(array &$elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as &$element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    foreach ($children as $child) $element['children'][] = $child;
//                    $element['children'][] = $children;
                } else $element['children'] = [];
                $branch[$element['id']] = $element;
                unset($element['id']);
            }
        }
        return $branch;
    }

    /**
     * Private - Category Tree
     * @param array $elements
     * @param int $parentId
     * @return array
     */
    private function categoryTree(array &$elements, $parentId = 0) {
        $cataegories = [];
        foreach ($elements as $element) {
            $childs = [];
            foreach ($elements as $child) {
                if(!is_null($child['parent_id'])) {
                    if($child['parent_id'] == $element['id']) {
                        $childs[] = [
                            'parent_id' => $element['id'],
                            'children_id' => $child['id'],
                            'children_name' => $child['name']
                        ];
                    }
                }
            }
            if(is_null($element['parent_id'])) {
                $cataegories[] = [
                    'id' => $element['id'],
                    'name' => $element['name'],
                    'children' => $childs
                ];
            }
        }
        return $cataegories;
    }

    /**
     * Public - PopSafe for CSR Campaign PopBox Neubodi Sept to 4 okt 2018
     */
    public function csrOrder (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['locker_id','locker_size','notes','item_photo'];

        $sessionId = $request->input('session_id'); //using mypopbox (no : 0170001234)
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $userId = $getUser->userId;
        $userDb = User::find($userId);
        if (!$userDb){
            $errorMessage = "Invalid User";
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $country = $userDb->country;
        if (!in_array($request->input('locker_id'), $this->forceAllowedLockers)){
            if($country == 'ID') {
                if ($this->forceClose){
                    $errorMessage = "We are sorry, this service is temporarily unavailable due to maintenance schedule. Please check our social media for further notice.";
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
            }
        }

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'locker_id' => 'required',
            'locker_size' => 'required|in:S,M,L,XL',
            'auto_extend' => 'nullable|in:0,1',
            'category_id' => 'nullable',
            'item_description' => 'nullable'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $sessionId = $request->input('session_id');
        $lockerId = $request->input('locker_id');
        $lockerSize = $request->input('locker_size');
        $notes = $request->input('notes');
        $itemPhoto = $request->input('item_photo');
        $autoExtend = $request->input('auto_extend', 1);
        $promoCode = $request->input('promo_code',null);
        $categoryId = $request->input('category_id',null);
        $itemDescription = $request->input('item_description',null);

        $expiredTime = $request->input('expired_time');
        
        // participant data
        $senderName = $request->input('sender_name');
        $senderEmail = $request->input('sender_email');
        $senderPhone = $request->input('sender_phone');
        $formParticipant = $request->input('form_participant');

        $request->request->add(['size' => $lockerSize]);

        $calculateOrder = $this->popSafeCalculate($request)->getOriginalContent();
        if ($calculateOrder->response->code!=200){
            $errorMessage = $calculateOrder->response->message;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $calculateData = $calculateOrder->data[0];
        $basePrice = $calculateData->price;
        $promoAmount = $calculateData->promo_amount;

//        $basePrice = 0;
//        $promoAmount = 0;

        // get user ID based on Session
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $userId = $getUser->userId;
        $userDb = User::find($userId);
        if (!$userDb){
            $errorMessage = 'Invalid User';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $userPhone = $userDb->phone;
        $userName = $userDb->name;
        $userEmail = $userDb->email;
        $country = $userDb->country;

        // get locker data
        $lockerModel = new LockerLocation();
        $lockerDb = $lockerModel->getLockerByLockerId($lockerId);
        if (empty($lockerDb)){
            $errorMessage = 'Invalid Locker';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        DB::beginTransaction();

        // create order
        $PopSafeCSRModel = new PopSafeCSR();
        $createCSROrder = $PopSafeCSRModel->createCSROrder($userId,$lockerId,$lockerSize,$basePrice,$autoExtend,$notes,$itemPhoto,$categoryId,$itemDescription);
        if (!$createCSROrder->isSuccess){
            $errorMessage = $createCSROrder->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $popSafeId = $createCSROrder->popSafeId;
        $pathPhoto = $createCSROrder->pathPhoto;
        $popSafeDb = $PopSafeCSRModel->find($popSafeId);
        $popSafeInvoice = $popSafeDb->invoice_code;
        $popSafeData = $createCSROrder->data;

        // create transaction
        $transactionModel = new Transaction();
        $createTransaction = $transactionModel->createPopsafeTransaction($userId,$popSafeId,$promoAmount);
        if (!$createTransaction->isSuccess){
            $errorMessage = $createTransaction->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $transactionId = $createTransaction->transactionId;

        $transactionDb = $transactionModel->find($transactionId);
        $paidAmount = $transactionDb->paid_amount;

        // debit transaction
        $balanceModel = new BalanceRecord();
        $debitBalance =$balanceModel->debitDeposit($userId,$paidAmount,$transactionId);
        if (!$debitBalance->isSuccess){
            DB::rollback();
            $errorMessage = $debitBalance->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // if promo code exist
        $promoName = null;
        $promoDescription = null;
        if ($promoCode){
            $params = [];
            $params['transaction_amount'] = $basePrice;
            $params['transaction_datetime'] = date('Y-m-d H:i:s');
            $params['locker_name'] = $lockerDb->name;
            $params['locker_size'] = $lockerSize;
            $params['item_amount'] = 1;
            $params['promo_code'] = $promoCode;
            $params['user_phone'] = $userPhone;
            $params['transaction_type'] = 'popsafe';
            $bypassPromo = false;

            $campaignResp = Promo::bookPromo($userPhone,'potongan',$transactionId,'',$params);
            if (!$campaignResp->isSuccess){
//                $errorMessage = $campaignResp->errorMsg;
//                return ApiHelper::buildResponse(400,$errorMessage);
                $bypassPromo = true;
            }
            if (!$bypassPromo){
                $promoName = $campaignResp->campaignName;
                $promoDescription = $campaignResp->campaignDescription;
                $campaignResp = Promo::claimPromo($userPhone,$transactionId);
                if (!$campaignResp->isSuccess){
                    $errorMessage = $campaignResp->errorMsg;
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
            }
        }

        // push to prox
        $apiProx = new ApiProx();
        $dataProx = [
            'userPhone' => $senderPhone,
            'userName'  => $senderName,
            'invoiceCode' => $popSafeInvoice,
            'address' => $popSafeDb->locker_name
        ];
        $orderProx = $apiProx->importPopSafe($dataProx);
        if (!$orderProx->isSuccess){
            $errorMessage = 'Failed To Push Locker';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $proxData = $orderProx->data;
        $expressId = $proxData->id;
        $popSafeDb = Popsafe::find($popSafeId);
        $popSafeDb->express_id = $expressId;
        $popSafeDb->save();

        if ($pathPhoto){
            $structure = 'media_/popsafe_/'.date('Y-M');
            if(!is_dir($structure)){
                mkdir($structure, 0777, true);
            }
            $decode = base64_decode($itemPhoto);
            file_put_contents($pathPhoto,$decode);
        }
        DB::commit();

        /*Send Email And SMS*/
        $qrCode = new QrCode();
        $qrCode
            ->setText($popSafeInvoice)
            ->setSize(200)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
            ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
            ->setLabel('Scan Qr Code')
            ->setLabelFontSize(13)
            ->setImageType(QrCode::IMAGE_TYPE_PNG);

        $popSafeData['email'] = $userEmail;
        $popSafeData['name'] = $userName;
        $popSafeData['QRcode'] = $qrCode;
        $popSafeData['amount'] = (float)$transactionDb->total_price;
        $popSafeData['paid_amount'] = (float)$transactionDb->paid_amount;
        $popSafeData['promo_amount'] = (float)$transactionDb->promo_amount;
        $popSafeData['promo_name'] = $promoName;
        $popSafeData['promo_description'] = $promoDescription;
        
        $popSafeData['participant_name'] = $senderName;
        $popSafeData['participant_phone'] = $senderPhone;
        $popSafeData['participant_email'] = $senderEmail;
        $popSafeData['participant_form'] = $formParticipant;

        //generate PDF specialy CSR
        $output = PDF::loadView('pdf.invoicecsr', $popSafeData);

        // send email to user/team 
        // please use mypopbox@popbox.asia registered in popsend member
       Mail::send('email.popsafe.csrcreated',$popSafeData, function($message) use ($popSafeData,$output){
        $message->subject('PopBox Neubodi #'.$popSafeData['invoice_id']);
            $message->to( $popSafeData['email'],$popSafeData['name']);
            $message->from('no-reply@popbox.asia','PopBox Neubodi - Notifier');
            $message->setBody($popSafeData['participant_form'], 'text/html');
            $message->attachData($output->output(),$popSafeData['invoice_id'].'.pdf');
        });

        // send email to participant
       Mail::send('email.popsafe.csrcreated',$popSafeData, function($message) use ($popSafeData,$output){
        $message->subject('PopBox Neubodi #'.$popSafeData['invoice_id']);
            $message->to( $popSafeData['participant_email'],$popSafeData['participant_name']);
            $message->from('no-reply@popbox.asia','PopBox Neubodi');
            $message->attachData($output->output(),$popSafeData['invoice_id'].'.pdf');
        });

        // send cc email to the team
        Mail::send('email.popsafe.csrcreated',$popSafeData, function($message) use ($popSafeData,$output){
        $message->subject('PopBox Neubodi #'.$popSafeData['invoice_id']);
        $message->to('developer@popbox.asia','Developer PopBox');
        $message->from('no-reply@popbox.asia','PopBox Neubodi - Notifier');
        $message->setBody($popSafeData['participant_form'], 'text/html');
        $message->attachData($output->output(),$popSafeData['invoice_id'].'.pdf');
        });

        if ($country == 'ID'){
            $smsMessage = "Hi $userName, Order anda berhasil. Gunakan Kode: $popSafeInvoice Untuk Membuka Locker Anda";

        } else {
            $smsMessage = "Hi $userName, You Have Successfully submit the order. Use this Code : $popSafeInvoice to Open the Locker";
        }

        // send SMS
        $sendSms = Helper::sendSms($userPhone,$smsMessage);

        //remove QRcode for return
        unset($popSafeData['QRcode']);
        return ApiHelper::buildResponse(200,'Order Success',[$popSafeData]);
    }

    public function lockerOrder (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['id', 'takeUserPhoneNumber','box_id','expressNumber','validateCode','paymentAmount',
            'paymentMethod', 'overdueTime', 'lockerSize', 'lockerNo', 'storeTime'];

        // check required
         $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
         if (!$checkRequired->isSuccess){
             $errorMessage = $checkRequired->errorMsg;
             return ApiHelper::buildResponse(400,$errorMessage);
         }

        /*Validate value from Input*/
         $rules = [
             'id' => 'required',
             'takeUserPhoneNumber' => 'required',
             'expressNumber' => 'required',
             'box_id' => 'required',
             'paymentAmount' => 'required',
             'validateCode' => 'required',
             'paymentMethod' => 'required',
             'overdueTime' => 'required',
             'storeTime' => 'required',
             'lockerNo' => 'required',
             'lockerSize' => 'required|in:S,M,MINI,L,XL'
         ];

         $validator = Validator::make($request->input(),$rules);
         if ($validator->fails()){
             $errors = $validator->errors()->all();
             $errorMessage = implode(' ',$errors);
             return ApiHelper::buildResponse(400,$errorMessage);
         }

        $invoiceCode = $request->input('expressNumber');
        $checkPopSafeLocker = new PopSafeLocker();
        $checkInvoiceCode = $checkPopSafeLocker->checkInvoice($invoiceCode);
        if (!$checkInvoiceCode->isSuccess){
            $errorMessage = 'Duplicate Entry of '.$invoiceCode;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // get locker location
        $lockerModel = new LockerLocation();
        $lockerDb = $lockerModel->getLockerByLockerId($request->input('box_id'));

        // get method Payment
        $paymentMethodModel = new PaymentMethod();
        $paymentMethodDb = $paymentMethodModel->getMethod($request->input('paymentMethod'));
        
        $expressId = $request->input('id'); 
        $pinCode = $request->input('validateCode');
        $storeTime = $request->input('storeTime');
        $overdueTime = $request->input('overdueTime');
        $lockerId = $request->input('box_id');
        $lockerSize = $request->input('lockerSize');
        $lockerNo = $request->input('lockerNo');
        $notes = $request->input('transactionRecord', 'PopSafe Locker - '.$invoiceCode);
        $itemDescription = "PopSafe $invoiceCode @".$lockerDb->name ." with ".$paymentMethodDb->data->name.' | '.$request->input('transactionRecord');
//        $expiredTime = date('Y-m-d H:i:s', $request->input('overdueTime')/1000);
        $basePrice = (float)$request->input('paymentAmount');
        $phoneNo = $request->input('takeUserPhoneNumber');
        $getUser = Users::getSessionIdByPhone($phoneNo);
        $sessionId = null;
        $paymentMethod = $request->input('paymentMethod');
        $tempCountry = 'ID';
        if (substr($phoneNo, 0, 2) == '01') $tempCountry = 'MY';
        
        if (!$getUser->isSuccess){
            $userDb = User::insertNewUser($phoneNo,$phoneNo,$phoneNo.'@email.com',$phoneNo,$tempCountry);
            if (!$userDb->isSuccess){
                $errorMessage = 'Failed To Create User';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $userId = $userDb->id;
            $memberId = $userDb->memberId;
        } else {
            $userId = $getUser->userId;
            $memberId = $getUser->memberId;
            if (!empty($getUser->sessionId)) $sessionId = $getUser->sessionId;
        }

        if (empty($sessionId)){
            $userSessionDb = UserSession::createUserSession($request->input('token'),$memberId);
            if (!$userSessionDb->isSuccess){
                $errorMessage = 'Failed To Create Session';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $sessionId = $userSessionDb->sessionId;
        }

        $userDb = User::find($userId);
        if (!$userDb){
            $errorMessage = "Invalid User";
            return ApiHelper::buildResponse(400,$errorMessage);
        }


        $userPhone = $userDb->phone;
        $userName = $userDb->name;
//        $userEmail = $userDb->email;
        $country = $userDb->country;
        $promoAmount = 0;

        // get user ID based on Session

        if (empty($lockerDb)){
            $errorMessage = 'Invalid Locker';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        if (!$paymentMethodDb->isSuccess) {
            $errorMessage = $paymentMethodDb->message;
            return ApiHelper::buildResponse(400,$errorMessage);            
        }
        $paymentMethodId = $paymentMethodDb->data->id;

        // get client id companies 
        $clienCompanies = new Companies();
        $companies = $clienCompanies->checkClientAvailable($invoiceCode);

        if (!$companies->isSuccess) {
            $errorMessage = $companies->message;
            return ApiHelper::buildResponse(400,$errorMessage);            
        }
        $companies = $companies->data;

        DB::beginTransaction();

        // create order
        $PopSafeLocker = new PopSafeLocker();
        $createLockerOrder = $PopSafeLocker->createOrderLocker($invoiceCode, $userId, $lockerId, $lockerSize,
            $basePrice, $overdueTime, $notes, $lockerNo, $pinCode, $storeTime, $itemDescription, $expressId);
        if (!$createLockerOrder->isSuccess){
            $errorMessage = 'Failed in Create Order From Locker';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $popSafeId = $createLockerOrder->popSafeId;
        $popSafeLocker = $createLockerOrder->lockerName;
//        $popSafeInvoice = $invoiceCode;
        $popSafeData = $createLockerOrder->data;

        $popSafeData['user_id'] = $userId;
        $popSafeData['phone'] = $userPhone;
        $popSafeData['session_id'] = $sessionId;

        // create transaction
        $transactionModel = new Transaction();
        $createTransaction = $transactionModel->createPopsafeLockerTransaction($notes,$userId,$popSafeId,$promoAmount,$itemDescription, $paymentMethodId, $companies);
        
        if (!$createTransaction->isSuccess){
            $errorMessage = "Failed To Create Transaction";
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // Creaet Payment
        $paymentModel = new Payment();
        $paymentTransaction = $paymentModel->createPayment($request, $paymentMethodId, $createTransaction, $userDb, $itemDescription);

        if (!$paymentTransaction->isSuccess){
            $errorMessage = "Failed To Create Payment Transaction";
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $popSafeData['transaction_id'] = $createTransaction->transactionId;

        DB::commit();

        if ($country == 'ID'){
            $smsMessage = "Hi $userName, Order Anda berhasil di PopBox @ $popSafeLocker. Kode PIN : $pinCode Untuk Mengambil Titipan Anda. Unduh Aplikasi PopBox di bit.do/getpopbox";
        } else {
            $smsMessage = "Hi $userName, Your Order is Successfully Created at $popSafeLocker. Pin Code : $pinCode to Collect Your Parcel. Get PopBox App from bit.do/getpopbox";
        }

        if (substr($invoiceCode, 0, 4) != "APXI") {
            // send SMS
            // $sendSms = Helper::sendSms($userPhone,$smsMessage);
        }


        return ApiHelper::buildResponse(200,'Order Success',[$popSafeData]);
    }
    
}
