<?php

namespace App\Http\Controllers\v1_1;

use App\Http\Library\ApiHelper;
use App\Http\Library\ApiPopExpress;
use App\Http\Library\Google;
use App\Http\Library\Helper;
use App\Http\Models\BalanceRecord;
use App\Http\Models\CategoryList;
use App\Http\Models\City;
use App\Http\Models\Country;
use App\Http\Models\Delivery;
use App\Http\Models\District;
use App\Http\Models\GoogleLocation;
use App\Http\Models\LockerLocation;
use App\Http\Models\Logs;
use App\Http\Models\MalaysiaTariff;
use App\Http\Models\PopExpress\Locker;
use App\Http\Models\Promo;
use App\Http\Models\Province;
use App\Http\Models\Transaction;
use App\Http\Models\UserSession;
use App\Jobs\ProcessExpressV2;
use App\Jobs\ProcessOderPopexpress;
use App\Jobs\PushMalaysiaPopSendLocker;
use App\Jobs\SendEmailOrder;
use App\Jobs\SendSMSEmailV2;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use CodeItNow\BarcodeBundle\Utils\QrCode;

class OrderController extends Controller
{
    /**
     * Calculate PopSend
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function popSendCalculate(Request $request){
        $errorMessage = null;
        $requiredInput = ['pickup_type','destinations'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // general rules for validation
        $rules = [
            'pickup_type' => 'required|in:locker,address',
            'destinations.*.type' => 'required|in:locker,address',
//            'destinations.*.name' => 'required_if:destination_data.*.type,address',
//            'destinations.*.address' => 'required_if:destination_data.*.type,address',
            'destinations.*.latitude' => 'required_if:destination_data.*.type,address',
            'destinations.*.longitude' => 'required_if:destination_data.*.type,address',
            'destinations.*.locker_id' => 'required_if:destination_data.*.type,locker',
            'destinations.*.weight' => 'nullable|numeric'
        ];

        $pickupType = $request->input('pickup_type');
        // rule validation based on  pickup type
        if ($pickupType == 'locker'){
            $rules['locker_id'] = 'required';
        } else {
            $rules['pickup_latitude'] = 'required';
            $rules['pickup_longitude'] = 'required';
//            $rules['pickup_name'] = 'required';
//            $rules['pickup_address'] = 'required';
        }

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $sessionId = $request->input('session_id');
        $getUserId = UserSession::getUserBySessionId($sessionId);
        if (!$getUserId->isSuccess){
            $errorMessage = $getUserId->erroMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUserId->userId;
        $userDb = User::find($userId);
        $country = $userDb->country;

        /*===========================Pickup Data===========================*/
        $pickupName = null;
        $pickupAddress = null;
        $pickupLatitude = null;
        $pickupLongitude = null;
        $pickupCity = null;
        $pickupCode = null;
        $pickupDistrictId = null;

        // initiate model
        $lockerModel = new LockerLocation();
        $lockerExpressModel = new Locker();

        // pickup Address
        if ($pickupType == 'address'){
            $pickupLatitude = $request->input('pickup_latitude');
            $pickupLongitude = $request->input('pickup_longitude');

            // get from reverse google
            $getPickupCity = $this->getAddressByReverseGoogle($pickupLatitude,$pickupLongitude,$country);
            if (!$getPickupCity->isSuccess){
                $errorMessage = 'Failed to Get Pickup Address. Please Try Again';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $pickupName = $request->input('pickup_name');
            $pickupAddress = $request->input('pickup_address');
            $googleAddressList = $getPickupCity->addressList;

            $checkPickup = $this->checkAddress($googleAddressList,$country, 'pickup');
            if (!$checkPickup->isSuccess){
                $errorMessage = $checkPickup->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $districtDb = District::with('city')
                ->where('id',$checkPickup->districtId)
                ->first();
            if ($districtDb) {
                $pickupCity = $districtDb->city->city_name;
                $pickupDistrictId = $districtDb->id;
            }
        }
        // pickup Locker
        elseif ($pickupType == 'locker') {
            $lockerId = $request->input('locker_id');

            if ($country == 'MY'){
                $lockerData = $lockerModel->getLockerByLockerId($lockerId);
                if (empty($lockerData)){
                    $errorMessage = 'Locker Not Found';
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
                $pickupName = $lockerData->name;
                $pickupAddress = "Locker $pickupName, $lockerData->address";
                $pickupLatitude = $lockerData->latitude;
                $pickupLongitude = $lockerData->longitude;
                $pickupCity = $lockerData->district;

                $districtName = $lockerData->kecamatan;
                $cityName = $lockerData->district;
            }
            else {
                // $lockerData = $lockerModel->getLockerByLockerId($lockerId);
                $lockerData = $lockerExpressModel->getLockerByLockerId($lockerId);
                if (empty($lockerData)){
                    $errorMessage = 'Locker Not Found';
                    return ApiHelper::buildResponse(400,$errorMessage);
                }

                $pickupName = $lockerData->name;
                $pickupAddress = "Locker $pickupName, $lockerData->address";
                $pickupLatitude = $lockerData->latitude;
                $pickupLongitude = $lockerData->longitude;
                $pickupCity = $lockerData->county;
                $districtName = $lockerData->district;
                $cityName = $lockerData->county;
            }

            // get district id
            $districtDb = District::with('city')
                ->where('district_name','LIKE',"$districtName")
                ->whereHas('city',function ($query) use ($cityName){
                    $query->where('city_name','LIKE',"%$cityName%");
                })->first();

            if ($districtDb){
                $pickupDistrictId = $districtDb->id;
            } else {
                // get data from gmaps based on latitude and longitude
                $getDestinationGoogle = $this->getAddressByReverseGoogle($lockerData->latitude,$lockerData->longitude,$country);

                if (!$getDestinationGoogle->isSuccess){
                    $errorMessage = 'Failed to Get Destination Locker Address. Please Try Again';
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
                $addressList = $getDestinationGoogle->addressList;
                $checkDestination = $this->checkAddress($addressList,$country, 'pickup');
                if (!$checkDestination->isSuccess){
                    $errorMessage = $checkDestination->errorMsg;
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
                $pickupDistrictId = $checkDestination->districtId;

            }
        }
        else {
            $errorMessage = 'Unknown Pickup Type';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        /*===========================End Pickup Data===========================*/

        /*==========================Destination Data==========================*/
        $destinationList = [];
        $destinationInput = $request->input('destinations');
        foreach ($destinationInput as $item){
            $item = (object)$item;
            $destinationType = $item->type;
            if ($destinationType == 'locker' && empty($item->locker_id)){
                $errorMessage = 'Locker Id Required for Destination Type Locker';
                return ApiHelper::buildResponse(400,$errorMessage);
//            } elseif ($destinationType == 'address' && (empty($item->latitude) || empty($item->longitude) || empty($item->address) || empty($item->name))){
            } elseif ($destinationType == 'address' && (empty($item->latitude) || empty($item->longitude))){
                $errorMessage = 'Destination Name, Destination Address, Latitude and Longitude Required for Destination Type Address';
                return ApiHelper::buildResponse(400,$errorMessage);
            } elseif (!in_array($destinationType,['locker','address'])) {
                $errorMessage = 'Unknown Destination Type';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $weight = 1;
            if (!empty($item->weight)) $weight = $item->weight;
            $tmp = new \stdClass();
            $tmp->destination_type = $item->type;
            $tmp->destination_name = null;
            $tmp->destination_address = null;
            $tmp->destination_latitude = null;
            $tmp->destination_longitude = null;
            $tmp->isValid = false;
            $tmp->error = null;
            $tmp->districtId = null;
            $tmp->weight = (double)$weight;
            $tmp->estimates_days = '';

            if ($destinationType == 'address'){
                $addressList = [];
                $destinationLatitude = $item->latitude;
                $destinationLongitude = $item->longitude;
                $getDestinationGoogle = $this->getAddressByReverseGoogle($destinationLatitude,$destinationLongitude,$country);
                if (!$getDestinationGoogle->isSuccess){
                    $tmp->error = 'Failed to Get Destination Address. Please Try Again';
                    $destinationList[] = $tmp;
                    continue;
                }
                $addressList = $getDestinationGoogle->addressList;
                $tmp->destination_latitude = $destinationLatitude;
                $tmp->destination_longitude = $destinationLongitude;
                $tmp->destination_name = $item->name;
                $tmp->destination_address = $item->address;

                // check based on list
                if ($country == 'MY') {
                    $checkDestination = $this->saveDestinationMalaysia($addressList);
                }
                else {
                    $checkDestination = $this->checkAddress($addressList, 'ID', 'destination');
                }

                if (!$checkDestination->isSuccess){
                    $tmp->error = $checkDestination->errorMsg;
                    $destinationList[] = $tmp;
                    continue;
                }
                $tmp->isValid = true;
                $tmp->districtId = (string)$checkDestination->districtId;
                $destinationList[] = $tmp;
            }
            elseif ($destinationType == 'locker'){
                $lockerId = $item->locker_id;
                if ($country == 'MY'){
                    $lockerData = $lockerModel->getLockerByLockerId($lockerId);
                    $districtName = $lockerData->kecamatan;
                    $cityName = $lockerData->district;
                    $provinceName = $lockerData->province;
                }
                else {
                    $lockerData = $lockerExpressModel->getLockerByLockerId($lockerId);
                    if (empty($lockerData)){
                        $errorMessage = 'Locker Not Found';
                        return ApiHelper::buildResponse(400,$errorMessage);
                    }
                    $districtName = $lockerData->district;
                    $cityName = $lockerData->county;
                    $provinceName = $lockerData->province;
                }
                $tmp->destination_name = $lockerData->name;
                $tmp->destination_address = "Locker $lockerData->name, $lockerData->address";
                $tmp->destination_latitude = $lockerData->latitude;
                $tmp->destination_longitude = $lockerData->longitude;

                // get district id
                $districtDb = District::with('city')
                    ->where('district_name','LIKE',"$districtName")
                    ->whereHas('city',function ($query) use ($cityName){
                        $query->where('city_name','LIKE',"%$cityName%");
                    })->first();

                if ($districtDb){
                    $tmp->districtId = (string)$districtDb->id;
                }
                else {
                    // get data from gmaps based on latitude and longitude
                    $getDestinationGoogle = $this->getAddressByReverseGoogle($lockerData->latitude,$lockerData->longitude,$country);
                    if (!$getDestinationGoogle->isSuccess){
                        $tmp->error = 'Failed to Get Destination Locker Address. Please Try Again';
                        $destinationList[] = $tmp;
                        continue;
                    }
                    $addressList = $getDestinationGoogle->addressList;
                    // check based on list
                    if ($country == 'MY') {
                        $checkDestination = $this->saveDestinationMalaysia($addressList);
                    }
                    else {
                        $checkDestination = $this->checkAddress($addressList, 'ID', 'destination');
                    }
                    if (!$checkDestination->isSuccess){
                        $tmp->error = $checkDestination->errorMsg;
                        $destinationList[] = $tmp;
                        continue;
                    }
                    $tmp->districtId = (string)$checkDestination->districtId;
                }
                if (!empty($tmp->districtId)){
                    $tmp->isValid = true;
                }
                $destinationList[] = $tmp;
            }
        }
        /*==========================End Destination Data==========================*/

        /*=========================CALCULATE TARIFF=========================*/
        $original_price =0;
        $totalPrice = 0;
        $promoAmount = 0;
        $promoName = null;
        $promoDescription = '';

        foreach ($destinationList as $item) {
            $item->regular_price = 0;
            $item->one_day_price = 0;
            $item->price = 0;
            $item->promo_amount = 0;

            if (!$item->isValid){
                continue;
            }

            $districtDb = District::with(['city','city.province'])->find($item->districtId);
            $districtCode = $districtDb->code;

            // if Malaysia
            if ($country == 'MY'){
                $provinceDestination = $districtDb->city->province->province_name;
                $cityDestination = $districtDb->city->city_name;
                $districtDestination = $districtDb->district_name;
                // query from malaysia tariff
                $malaysiaTariffModel = new MalaysiaTariff();
                $calculate = $malaysiaTariffModel->calculateTariff($pickupCity,$provinceDestination,$cityDestination,$districtDestination,$item->weight);

                if (!$calculate->isSuccess){
                    $item->isValid = false;
                    if (empty($item->error)){
                        $item->error = 'Failed to Calculate Tariff';
                    }
                    continue;
                }


                $price = (float)$calculate->totalPrice;
                $promoPrice = 0;

                $item->price = $price;
                $item->promo_amount = 0;
                $item->regular_price = $price;
                $item->one_day_price = 0;
                $item->estimates_days = $calculate->est_delivery;
                $original_price += $price;
                $totalPrice += $price * $item->weight;
                $promoAmount += $promoPrice;
            }
            // if else malaysia
            else {
                // check origin pickup
                $cityOrigin = Cache::get('express-city-origin',null);
                $popExpressAPI = new ApiPopExpress();
                if (empty($cityOrigin)){
                    // get from API
                    $getCity = $popExpressAPI->getCityOrigin();
                    if (empty($getCity)){
                        $message = 'Failed Get City';
                        $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                        return response()->json($resp);
                    }
                    if ($getCity->response->code !=200){
                        $message = $getCity->response->message;
                        $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                        return response()->json($resp);
                    }
                    $cityOrigin = $getCity->data->origins;
                    Cache::put('express-city-origin',$cityOrigin,3600);
                }
                if (empty($cityOrigin)){
                    $errorMessage = 'Empty City Origin';
                    return ApiHelper::buildResponse(400,$errorMessage);
                }

                $isOrigin = null;
                foreach ($cityOrigin as $cityItem) {
                    $tmpName = strtoupper($cityItem->name);
                    $tmpCity = strtoupper($pickupCity);
                    if (strpos($tmpCity,$tmpName) !== false) {
                        $isOrigin = $cityItem->name;
                        $pickupCode =$cityItem->code;
                    }
                }

                if (empty($isOrigin) && $pickupType == 'address'){
                    $errorMessage = 'Origin Out of Coverage';
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
                $pickupCity = $isOrigin;

                $expressCalculate = $popExpressAPI->calculateByCode($pickupCity,$pickupCode,$item->destination_type,$districtCode,$item->destination_name);
                if (empty($expressCalculate)){
                    $item->isValid = false;
                    if (empty($item->error)){
                        $item->error = 'Failed to Calculate Tariff';
                    }
                    continue;
                }

                if ($expressCalculate->response->code != 200){
                    $item->isValid = false;
                    if (empty($item->error)){
                        $item->error = 'Failed to Calculate Tariff';
                    }
                    continue;
                }

                if (empty($expressCalculate->data)){
                    $item->isValid = false;
                    if (empty($item->error)){
                        $item->error = 'Out Of Coverage by PopExpress';
                    }
                    continue;
                }
                $tariffData = $expressCalculate->data->tariffs[0];

                // get regular tariff
                $regularPrice = 0;
                $oneDayPrice = 0;
                foreach ($tariffData as $tariffDatum) {
                    $item->estimates_days = $tariffDatum->estimation;
                    if ($tariffDatum->type == 'regular'){
                        $regularPrice = $tariffDatum->price;

                    }
                    if ($tariffDatum->type == 'one_day'){
                        $oneDayPrice = $tariffDatum->price;
                    }
                }
                // if regular price not found
                if (empty($regularPrice) && empty($oneDayPrice)){
                    $item->isValid = false;
                    if (empty($item->error)){
                        $item->error = 'Out Of Coverage Price by PopExpress';
                    }
                    continue;
                }
                $item->regular_price = $regularPrice;
                $item->one_day_price = $oneDayPrice;

                $selectedPrice = $regularPrice;
                $discountAmount = 0;
                $price = $item->weight * $selectedPrice;

                $item->promo_amount = $discountAmount;
                $original_price +=$price;
                $item->price = $price;
                $totalPrice += $price;
                $promoAmount += $discountAmount;
            }
        }

        /*=========================END CALCULATE TARIFF=========================*/

        /*=========================CALCULATE PROMO=========================*/
        $promoCode = $request->input('promo_code');
        $phone = $userDb->phone;
        $params = [];
        $params['transaction_amount'] = $totalPrice;
        $params['transaction_datetime'] = date('Y-m-d H:i:s');
        $params['origin_locker_name'] = $pickupName;
        $params['item_amount'] = \count($destinationList) ;
        $params['promo_code'] = $promoCode;
        $params['user_phone'] = $phone;
        $params['transaction_type'] = 'popsend';
        $params['pickup_type'] = $pickupType;
        $params['origin_district_id'] = $pickupDistrictId;

        $campaignResp = Promo::checkPromo($phone,'potongan',$params);
        $campaignName = null;
        $discAmount = null;
        if ($campaignResp->isSuccess) {
            $totalPrice = $campaignResp->totalPrice;
            $promoName = $campaignResp->campaignName;
            $promoAmount = $campaignResp->totalDiscount;
            $promoDescription = $campaignResp->campaignDescription;
        }
        /*=========================END CALCULATE PROMO=========================*/

        // create response
        $response = new \stdClass();
        $response->pickup_type = $pickupType;
        $response->pickup_name = $pickupName;
        $response->pickup_address = $pickupAddress;
        $response->pickup_latitude = $pickupLatitude;
        $response->pickup_longitude = $pickupLongitude;
        $response->districtId = (string)$pickupDistrictId;
        $response->original_price = $original_price;
        $response->total_price = $totalPrice;
        $response->promo_amount = $promoAmount;
        $response->promo_name = $promoName;
        $response->promo_description = $promoDescription;
        $response->destinations = $destinationList;

        return ApiHelper::buildResponse(200,null,$response);
    }

    /**
     * Submit PopSend
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function popSendSubmit(Request $request){
        $location = "";
        $errorMessage = null;
        $requiredInput = ['pickup_type','destinations'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        Logs::logFile($location,"popSendSubmit","Begin Order PopSend");
        Logs::logFile($location,"popSendSubmit","Params: ".json_encode($request->input()));

        // general rules for validation
        $rules = [
            'pickup_type' => 'required|in:locker,address',
            'sender_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'sender_phone' => 'required|numeric|digits_between:10,15',
            'pickup_notes' => 'nullable|string',
            'item_photo' => 'required',
            "pickup_address_detail" => 'nullable|string',
            'destinations.*.type' => 'required|in:locker,address',
            'destinations.*.name' => 'required_if:destination_data.*.type,address',
            'destinations.*.address' => 'required_if:destination_data.*.type,address',
            'destinations.*.address_detail' => 'nullable|string',
            'destinations.*.latitude' => 'required_if:destination_data.*.type,address',
            'destinations.*.longitude' => 'required_if:destination_data.*.type,address',
            'destinations.*.locker_id' => 'required_if:destination_data.*.type,locker',
            'destinations.*.weight' => 'required|numeric',
            'destinations.*.recipient_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'destinations.*.recipient_phone' => 'required|numeric|digits_between:10,15',
            'destinations.*.category_id' => 'required',
            'destinations.*.item_description' => 'nullable|string',
            'promo_code' => 'nullable|string'
        ];

        $pickupType = $request->input('pickup_type');
        $destinationInput = $request->input('destinations');
        $sessionId = $request->input('session_id');
        $categoryId = $request->input('category_id');

        // rule validation based on  pickup type
        if ($pickupType == 'locker'){
            $rules['locker_id'] = 'required';
        } else {
            $rules['pickup_latitude'] = 'required';
            $rules['pickup_longitude'] = 'required';
            $rules['pickup_name'] = 'required';
            $rules['pickup_address'] = 'required';
        }

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $getUserId = UserSession::getUserBySessionId($sessionId);
        if (!$getUserId->isSuccess){
            $errorMessage = $getUserId->erroMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUserId->userId;
        $userDb = User::find($userId);
        $country = $userDb->country;
        $userPhone = $userDb->phone;

        // calculate price
        $calculate = $this->popSendCalculate($request)->getOriginalContent();

        if ($calculate->response->code!=200){
            $errorMessage = $calculate->response->message;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        if (empty($calculate->data[0])){
            $errorMessage = 'Failed to Calculate Data';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $calculateData = $calculate->data[0];

        DB::beginTransaction();
        // create delivery order
        $deliveryModel = new Delivery();
        $createDelivery = $deliveryModel->createOrder($userId,$calculateData,$request);
        if (!$createDelivery->isSuccess){
            DB::rollback();
            $errorMessage = $createDelivery->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $deliveryId = $createDelivery->deliveryId;
        $promoAmount = $calculateData->promo_amount;
        $pathPhoto = $createDelivery->pathPhoto;
        $itemPhoto = $request->input('item_photo');

        if ($pathPhoto){
            $structure = 'media_/popsend_/'.date('Y-M');
            if(!is_dir($structure)){
                mkdir($structure, 0777, true);
            }
            $decode = base64_decode($itemPhoto);
            file_put_contents($pathPhoto,$decode);
        }

        // submit transaction
        $transactionModel = new Transaction();
        $createTransaction = $transactionModel->createDeliveryTransaction($userId,$deliveryId,$promoAmount);

        if (!$createTransaction->isSuccess){
            DB::rollback();
            $errorMessage = $createTransaction->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $transactionId = $createTransaction->transactionId;

        // get transaction by Id
        $transactionDb = $transactionModel->find($transactionId);

        $paidAmount = $transactionDb->paid_amount;

        // debit transaction
        $balanceModel = new BalanceRecord();
        $debitBalance =$balanceModel->debitDeposit($userId,$paidAmount,$transactionId);

        if (!$debitBalance->isSuccess){
            DB::rollback();
            $errorMessage = $debitBalance->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $deliveryData = $deliveryModel->find($deliveryId);
        $destinations = $deliveryData->destinations;

        $params = [];
        $params['transaction_amount'] = $calculate->data[0]->original_price;
        $params['transaction_datetime'] = date('Y-m-d H:i:s');
        $params['origin_locker_name'] = $calculate->data[0]->pickup_name;
        $params['item_amount'] = \count($calculate->data[0]->destinations);
        $params['promo_code'] = $request->input('promo_code');
        $params['user_phone'] = $userPhone;
        $params['pickup_type'] = $pickupType;
        $params['transaction_type'] = 'popsend';
        $params['origin_district_id'] = $calculate->data[0]->districtId;

        $campaignResp = Promo::bookPromo($userPhone,'potongan',$transactionId,'',$params);

        $campaignResp = Promo::claimPromo($userPhone,$transactionId);

        // create response
        $destinationList = [];
        foreach ($destinations as $destination) {
            $tmp = new \stdClass();
            $qrCode = new QrCode();
            $qrCode
                ->setText($destination->invoice_sub_code)
                ->setSize(200)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel('')
                ->setLabelFontSize(13)
                ->setImageType(QrCode::IMAGE_TYPE_PNG);

            $tmp->destination_type = $destination->destination_type;
            $tmp->sub_invoice_id = $destination->invoice_sub_code;
            $tmp->destination_name = $destination->destination_name;
            $tmp->destination_address = $destination->destination_address;
            $tmp->destination_latitude = $destination->destination_latitude;
            $tmp->destination_longitude = $destination->destination_longitude;
            $tmp->detail = $destination->destination_address_detail;
            $tmp->status = $destination->status;
            $tmp->isValid = true;
            $tmp->error = "";
            $tmp->districtId = $destination->district_id;
            $tmp->weight = (float)$destination->weight;
            $tmp->actual_weight = (float)$destination->actual_weight;
            $tmp->regular_price = 0;
            $tmp->one_day_price = 0;
            $tmp->price = $destination->price;
            $tmp->promo_amount = 0;
            $tmp->getContentType = $qrCode->getContentType();
            $tmp->QRcode = $qrCode->generate();
            $tmp->notes = $destination->notes;
            $tmp->name = $destination->name_recipient;
            $tmp->phone = $destination->phone_recipient;
            $tmp->item_description = $destination->category;
            $destinationList[] = $tmp;
        }

        // update calculate data
        $calculateData->sender_phone = $deliveryData->phone_sender;
        $calculateData->notes = $deliveryData->notes;
        $calculateData->detail = $deliveryData->origin_address_detail;
        $calculateData->type_pickup = $deliveryData->pickup_type;

        $calculateData->sender_name = $deliveryData->name_sender;
        $calculateData->pickup_date = $deliveryData->pickup_date;
        $calculateData->invoice_id = $deliveryData->invoice_code;
        $calculateData->destinations = $destinationList;
        //Commit Transaction After Success
        DB::commit();

        Logs::logFile($location,"popSendSubmit","Begin dispatch PopExpress V2");
        $this->dispatch(new ProcessExpressV2($deliveryData));

        /*Send Email And SMS*/
        Logs::logFile($location,"popSendSubmit","Begin dispatch sms V2");
        $this->dispatch((new SendSMSEmailV2($deliveryData,$calculateData)));

        if ($country!='ID'){
            // process to prox
            $this->dispatch(new PushMalaysiaPopSendLocker($deliveryData));
        }

        foreach ($calculateData->destinations as $destination) {
            unset($destination->getContentType);
            unset($destination->QRcode);
        }
        return ApiHelper::buildResponse(200,null,$calculateData);
    }

    /*=================Private Function=================*/

    /**
     * Get Address based on Latitude Longitude
     * @param $latitude
     * @param $longitude
     * @param string $country
     * @return \stdClass
     */
    private function getAddressByReverseGoogle($latitude, $longitude,$country = 'ID'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->address = null;
        $response->addressList = [];

        // reverse geocode
        $googleApi = new Google();
        $reverseGeocode = $googleApi->reverseGeocode($latitude,$longitude);
        if (empty($reverseGeocode)){
            $response->errorMsg = 'Failed to Get Geocode';
            return $response;
        }
        if ($reverseGeocode->status!='OK'){
            $response->errorMsg = 'Failed OK to Get Geocode';
            return $response;
        }
        $results = $reverseGeocode->results;

        $addressResult = $results[0];

        // get address results
        foreach ($results as $result) {
            $types = $result->types;
            if (in_array('street_address',$types)){
                $addressResult = $result;
            }
        }

        // get formatted address
        $addressName = $addressResult->formatted_address;

        $addressList = [];

        // get district, city, province name list
        foreach ($results as $result) {
            $addressComponent = $result->address_components;
            $tmp = new \stdClass();
            $tmp->district = null;
            $tmp->city = null;
            $tmp->province = null;

            foreach ($addressComponent as $value) {
                $types = $value->types;
                $name = $value->long_name;
                if ($country == 'MY') {
                    // insert into district
                    if (in_array('sublocality',$types)){
                        $tmp->district = $name;
                    }
                    // insert into city
                    if (in_array('locality',$types)){
                        $tmp->city = $name;
                    }
                    // insert into provinces
                    if (in_array('administrative_area_level_1',$types)){
                        $tmp->province = $name;
                    }
                } else {
                    // insert into district
                    if (in_array('administrative_area_level_3',$types)){
                        $tmp->district = $name;
                    }
                    // insert into city
                    if (in_array('administrative_area_level_2',$types)){
                        $tmp->city = $name;
                    }
                    // insert into provinces
                    if (in_array('administrative_area_level_1',$types)){
                        $tmp->province = $name;
                    }
                }
            }
            $addressList[] = $tmp;
        }

        $response->isSuccess = true;
        $response->address = $addressName;
        $response->addressList = $addressList;
        return $response;
    }

    /**
     * Check Address from Google Location
     * @param array $addressList
     * @param string $country
     * @return \stdClass
     */
    private function checkAddress($addressList = [],$country='ID', $type)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->districtId = null;
        $response->districtCode = null;
        $response->cityId = null;

        $districtId = null;
        if ($country == 'MY') {
            foreach ($addressList as $item) {
                $districtName = $item->district;
                if(is_null($districtName)) {
                    $districtName = $item->city;
                }
                $cityName = $item->city;
                $provinceName = $item->province;
                if (empty($cityName) || empty($provinceName)) continue;

                $checkAddress = District::with('city', 'city.province')
                    ->where('district_name', $districtName)
                    ->whereHas('city', function ($query) use ($cityName) {
                        $query->where('city_name', $cityName);
                    })->whereHas('city.province', function ($query) use ($provinceName) {
                        $query->where('province_name', $provinceName);
                    })->first();

                if ($checkAddress) {
                    $districtId = $checkAddress->id;
                    break;
                }
            }
        } else {
            foreach ($addressList as $item) {
                $districtName = $item->district;
                $cityName = $item->city;
                $provinceName = $item->province;
                if (empty($districtName) || empty($cityName) || empty($provinceName)) continue;

                // check on existing db
                $checkAddress = District::with('city', 'city.province')
                    ->where('district_name', $districtName)
                    ->whereHas('city', function ($query) use ($cityName) {
                        $query->where('city_name', $cityName);
                    })->whereHas('city.province', function ($query) use ($provinceName) {
                        $query->where('province_name', $provinceName);
                    })->first();

                if ($checkAddress) {
                    $districtId = $checkAddress->id;
                    break;
                }

                // query on  google location existing database
                $checkGoogleDb = GoogleLocation::where('district', $districtName)
                    ->where('city', $cityName)
                    ->where('province', $provinceName)
                    ->first();

                if ($checkGoogleDb) {
                    if (!empty($checkGoogleDb->district_id)) {
                        $districtId = $checkGoogleDb->district_id;
                        break;
                    } else {
                        break;
                    }
                }

                // check based on city
                $tmpCity = $cityName;
                if (strpos($tmpCity, 'Jakarta') !== false) {
                    $tmpCity = 'Jakarta';
                }
                if (strpos($tmpCity, 'Kota ') !== false) {
                    $tmpCity = str_replace('Kota ', '', $tmpCity);
                }

                if (strpos($tmpCity, 'Kabupaten ') !== false) {
                    $tmpCity = str_replace('Kabupaten ', '', $tmpCity);
                }

                if (strpos($tmpCity, ' City') !== false) {
                    $tmpCity = str_replace(' City', '', $tmpCity);
                }

                if (strpos($tmpCity, ' Regency') !== false) {
                    $tmpCity = str_replace(' Regency', '', $tmpCity);
                }

                // check district
                $tmpDistrict = $districtName;
                if (strpos($tmpDistrict, 'Kecamatan') !== false) {
                    $tmpDistrict = str_replace('Kecamatan ', '', $tmpDistrict);
                }
                if (strpos($tmpDistrict, 'tanahabang') !== false && $tmpCity == 'Jakarta') {
                    $tmpDistrict = 'Tanah Abang';
                }
                if (strpos($tmpDistrict, 'Grogol petamburan') !== false && $tmpCity == 'Jakarta') {
                    $tmpDistrict = 'Grogol';
                }

                $districtDb = District::with('city')
                    ->where('district_name', 'LIKE', $tmpDistrict)
                    ->whereHas('city', function ($query) use ($tmpCity) {
                        $query->where('city_name', 'LIKE', "%$tmpCity%");
                    })
                    ->get();

                if (count($districtDb) == 1) {
                    $districtDb = $districtDb->first();
                    $districtId = $districtDb->id;
                }

                // insert into db
                $googleDb = new GoogleLocation();
                $googleDb->district = $districtName;
                $googleDb->city = $cityName;
                $googleDb->province = $provinceName;
                $googleDb->district_id = $districtId;
                $googleDb->save();
            }
        }

        if (empty($districtId)) {
            $response->errorMsg = 'Out Of Coverage Coverage';
            return $response;
        }

        if ($type == 'pickup') {
            // check if active pickup Delivery
            $districtDb = District::with('city', 'city.province')
                ->find($districtId);

            if ($type == 'pickup') {
                // check if active pickup Delivery
                $districtDb = District::with('city', 'city.province')
                    ->find($districtId);

                $districtPickup = $districtDb->pickup_available;
                $cityPickup = $districtDb->city->pickup_available;
                $provincePickup = $districtDb->city->province->pickup_available;

                if (!empty($districtPickup)) {
                    $response->isSuccess = true;
                    $response->districtId = $districtId;
                    return $response;
                }
                if (!empty($cityPickup)) {
                    $response->isSuccess = true;
                    $response->districtId = $districtId;
                    return $response;
                }
                if (!empty($provincePickup)) {
                    $response->isSuccess = true;
                    $response->districtId = $districtId;
                    return $response;
                }

                $response->errorMsg = 'Out Of Coverage';
                return $response;
            }

        } else if ($type == 'destination') {
            if (!empty($districtId)) {
                $response->isSuccess = true;
                $response->districtId = $districtId;
                return $response;
            }
        }
    }

    /**
     * Save Destination malaysia
     * @param $addressList
     * @return \stdClass
     */
    private function saveDestinationMalaysia($addressList){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->districtId = null;

        foreach ($addressList as $value) {
            $districtName = $value->district;
            $cityName = $value->city;
            $provinceName = $value->province;

            if (empty($districtName) || empty($cityName) || empty($provinceName)) continue;

            $countryDb = Country::where('country_name','Malaysia')->first();
            if (!$countryDb){
                $countryDb = new Country();
                $countryDb->country_name = 'Malaysia';
                $countryDb->save();
            }
            $countryId = $countryDb->id;
            $provinceDb = Province::where('province_name',$provinceName)->where('country_id',$countryId)->first();
            if (!$provinceDb){
                $provinceDb = new Province();
                $provinceDb->province_name = $provinceName;
                $provinceDb->country_id = $countryId;
                $provinceDb->save();
            }
            $provinceId = $provinceDb->id;

            $cityDb = City::where('city_name',$cityName)->where('province_id',$provinceId)->first();
            if (!$cityDb){
                $cityDb = new City();
                $cityDb->city_name = $cityName;
                $cityDb->province_id = $provinceId;
                $cityDb->save();
            }
            $cityId = $cityDb->id;

            $districtDb = District::where('district_name',$districtName)->where('city_id',$cityId)->first();
            if (!$districtDb){
                $districtDb = new District();
                $districtCode = "MY".Helper::generateRandomString(3,'Numeric');
                $districtDb->city_id = $cityId;
                $districtDb->code = $districtCode;
                $districtDb->district_name = $districtName;
                $districtDb->save();
            }

            $districtId = $districtDb->id;
        }
        if (!empty($districtId)){
            $response->isSuccess = true;
            $response->districtId = $districtId;
        }

        return $response;
    }
}
