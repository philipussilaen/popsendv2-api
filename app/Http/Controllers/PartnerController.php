<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 26/06/18
 * Time: 22.38
 */

namespace App\Http\Controllers;

use App\Http\Library\ApiHelper;
use App\Http\Models\Partner\Partner;
use App\Http\Models\UserSession;
use Illuminate\Http\Request;

class PartnerController extends Controller {

    public function getPartnerList(Request $request) {

        $errorMessage = null;
        $requiredInput = ['session_id'];

        $sessionId = $request->input('session_id');

        //check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // Get User
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $result = [];

        $partner = Partner::all();

        if (empty($partner)) {
            $errorMessage = 'Partner Not Found';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        foreach ($partner as $p) {
            $temp = new \stdClass();
            $temp->partner_code = $p->code;
            $temp->partner_name = $p->name;
            $temp->partner_description = $p->description;
            $result[] = $temp;
        }

        return ApiHelper::buildResponse(200,null,$result);
    }

    public function getPartnerHowTo(Request $request) {

        $errorMessage = null;
        $requiredInput = ['session_id', 'partner_code'];

        $sessionId = $request->input('session_id');
        $partnerCode = $request->input('partner_code');

        //check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // Get User
        $getUser = UserSession::getUserBySessionId($sessionId);
        if (!$getUser->isSuccess){
            $errorMessage = $getUser->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $partner = Partner::where('code', '=', $partnerCode)->first();

        if (empty($partner)) {
            $errorMessage = 'Partner Not Found';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $result = null;
        $howtolist = [];
        $howtodetaillist = [];

        $resultt = Array();
        foreach ($partner->howto as $howTo) {
            $resultt[$howTo->title][] = $howTo->description;
        }

        foreach ($resultt as $key => $res) {
            $temp = new \stdClass();
            foreach ($res as $keyy => $r) {
                $tempp = new \stdClass();
                $tempp->description = $r;
                $howtodetaillist[] = $tempp;
            }
            $temp->how_to_title = $key;
            $temp->how_to_desc = $howtodetaillist;
            $howtolist[] = $temp;
            $howtodetaillist = null;
        }

        $result = $howtolist;
        return ApiHelper::buildResponse(200,null,$result);
    }
}