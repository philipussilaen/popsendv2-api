<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        Log::error($e);
        $code = 500;
        $message = 'Something Went Wrong';
        $response = new \stdClass();
        $response->code = $code;
        $response->message = $message;

        if ($e instanceof MethodNotAllowedHttpException){
            $code = 405;
            $message = 'Method Not Allowed';

            $response->code = $code;
            $response->message = $message;
        } elseif ($e instanceof NotFoundHttpException){
            $code = 404;
            $message = 'URL Not Found';

            $response->code = $code;
            $response->message = $message;
        }

        $result = new \stdClass();
        $result->response = $response;
        $result->data = [];

        return response()->json($result);

        // return parent::render($request, $e);
    }
}
