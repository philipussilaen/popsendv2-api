<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forgot Password</title>

    <style>
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
            padding: 2px;
        }
    </style>
</head>
<body>

<p>Hai {{$name}}</p>
<p>
    Anda telah melakukan permintaan untuk perubahan password pada akun {{$phone}},

    berikut adalah Kode Unik untuk ubah kata Sandi Anda
</p>

<h3>{{ $code }}</h3>

<p>
    Masukkan Kode diatas untuk ubah kata sandi Anda.
</p>

<p>Jika anda mengalami kesulitan harap hubungi info@popbox.asia atau 021 - 22538719 </p>

<br><br>
<p>
    Salam Hangat, <br>
    <br>
    PopBox Asia <br>
    Jl. Palmerah Utara III No.62 FGH <br>
    (samping SPBU Palmerah Utara)<br>
    Jakarta Barat 11480<br>
    www.popbox.asia <br>
    Tlp.021-22538719 <br>
    <a href="www.popbox.asia">www.popbox.asia</a>
</p>


</body>
</html>