<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDeliveryDeliveryDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->string('origin_address_detail',512)->nullable()->after('origin_address');
        });
        Schema::table('delivery_destinations', function (Blueprint $table) {
            $table->string('destination_address_detail',512)->nullable()->after('destination_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn('origin_address_detail');
        });
        Schema::table('delivery_destinations', function (Blueprint $table) {
            $table->dropColumn('destination_address_detail');
        });
    }
}
