<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInfoArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_article', function (Blueprint $table) {
            $table->increments('article_id');
            $table->string('title');
            $table->string('subtitle');
            $table->string('content');
            $table->string('image');
            $table->string('article_type_id');
            $table->string('article_status_id');
            $table->string('article_priority');
            $table->string('article_country_id');
            $table->string('start_date');
            $table->string('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
