<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPopsafesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->dateTime('cancellation_time')->after('transaction_date')->nullable();
            $table->dateTime('last_extended_datetime')->after('cancellation_time')->nullable();

            $table->smallInteger('auto_extend')->after('status')->default(0);
            $table->smallInteger('number_of_extend')->after('notes')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->dropColumn('cancellation_time');
            $table->dropColumn('last_extended_datetime');
            $table->dropColumn('auto_extend');
            $table->dropColumn('number_of_extend');
        });
    }
}
