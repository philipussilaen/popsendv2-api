<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTypeOnTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->integer('transaction_id_reference',false,true)->comment('Primary Key/ID of delivery/safe/referral')->change();
            $table->string('invoice_id',128)->after('user_id')->comment('Number Transaction invoice');
            $table->string('transaction_type',64)->comment('Transaction Type, delivery,safe,referral,topup,refund')->change();
            $table->string('status',32)->comment('transaction status, waiting, paid, refund')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->integer('transaction_id_reference',false,false)->change();
            $table->dropColumn('invoice_id');
            $table->string('transaction_type',255)->change();
            $table->string('status',45)->change();
        });
    }
}
