<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixNullableCampaignUsages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_usages', function (Blueprint $table) {
          \Illuminate\Support\Facades\DB::statement("ALTER TABLE `campaign_usages` ALTER `voucher_id` DROP DEFAULT;");
          \Illuminate\Support\Facades\DB::statement("ALTER TABLE `campaign_usages` CHANGE COLUMN `voucher_id` `voucher_id` INT(10) UNSIGNED NULL AFTER `campaign_id`;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_usages', function (Blueprint $table) {

        });

    }
}
