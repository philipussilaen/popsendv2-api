<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreeGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_gifts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id', false, true);
            $table->string('occupation');
            $table->date('birtd_date');
            $table->string('gender');
            $table->string('locker_name');
            $table->string('locker_address');
            $table->string('locker_address_detail');
            $table->string('operation_hours');
            $table->string('locker_number')->nullable();
            $table->string('code_pin')->nullable();
            $table->string('invoice_id')->nullable();
            $table->string('prox_id')->nullable();
            $table->string('status')->default('On Process');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('free_gifts');
    }
}
