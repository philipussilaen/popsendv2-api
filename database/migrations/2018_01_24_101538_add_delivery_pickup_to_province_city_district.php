<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryPickupToProvinceCityDistrict extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provinces', function (Blueprint $table) {
            $table->integer('pickup_available',false,false)->default(0)->after('province_name');
            $table->integer('delivery_available',false,false)->default(0)->after('pickup_available');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->integer('pickup_available',false,false)->nullable()->after('city_name');
            $table->integer('delivery_available',false,false)->nullable()->after('pickup_available');
        });
        Schema::table('districts', function (Blueprint $table) {
            $table->integer('pickup_available',false,false)->nullable()->after('district_name');
            $table->integer('delivery_available',false,false)->nullable()->after('pickup_available');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('provinces', function (Blueprint $table) {
            $table->dropColumn('pickup_available');
            $table->dropColumn('delivery_available');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('pickup_available');
            $table->dropColumn('delivery_available');
        });
        Schema::table('districts', function (Blueprint $table) {
            $table->dropColumn('pickup_available');
            $table->dropColumn('delivery_available');
        });
    }
}
