<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('user_id',false,true);
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->integer('district_id',false,true);
            $table->string('pickup_type',45);
            $table->string('origin_name',250);
            $table->string('origin_address',250);
            $table->string('origin_latitude',45);
            $table->string('origin_longitude',45);
            $table->string('origin_locker_size',45);
            $table->text('item_photo');
            $table->dateTime('pickup_date');
            $table->string('invoice_code',45);
            $table->string('status',45);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
