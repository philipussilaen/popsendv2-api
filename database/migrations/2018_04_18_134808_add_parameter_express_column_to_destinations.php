<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParameterExpressColumnToDestinations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_destinations', function (Blueprint $table) {
            $table->text('express_url')->nullable()->after('price');
            $table->text('express_parameter')->nullable()->after('express_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_destinations', function (Blueprint $table) {
            $table->dropColumn('express_url');
            $table->dropColumn('express_parameter');
        });
    }
}
