<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_destinations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('delivery_id',false,true);
            $table->foreign('delivery_id')->references('id')->on('deliveries');

            $table->integer('district_id',false,true);

            $table->string('destination_type',45);
            $table->string('invoice_sub_code',45);
            $table->string('destination_name',250);
            $table->string('destination_address',250);
            $table->string('destination_latitude',45);
            $table->string('destination_longitude',45);
            $table->string('status',45);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_destinations');
    }
}
