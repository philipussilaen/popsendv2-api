<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMalaysiaTariffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('malaysia_tariff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country',8);
            $table->text('origin')->nullable();
            $table->string('destination_province',128)->nullable();
            $table->string('destination_city',128)->nullable();
            $table->string('destination_district',128)->nullable();
            $table->decimal('doc_price',12,2)->default(0);
            $table->decimal('s_price',12,2)->default(0);
            $table->decimal('m_price',12,2)->default(0);
            $table->decimal('l_price',12,2)->default(0);
            $table->string('est_delivery',128);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('malaysia_tariff');
    }
}
