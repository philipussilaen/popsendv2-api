<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropIsColumnTablePopsafe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->dropColumn('is_cancel');
            $table->dropColumn('is_expired');
            $table->dropColumn('is_collect');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->smallInteger('is_cancel')->after('auto_extend')->default(1);
            $table->smallInteger('is_expired')->after('is_cancel')->default(1);
            $table->smallInteger('is_collect')->after('is_expired')->default(0);
        });
    }
}
