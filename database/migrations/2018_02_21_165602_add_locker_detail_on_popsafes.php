<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLockerDetailOnPopsafes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->string('locker_address_detail')->nullable()->after('locker_address');
            $table->string('operational_hours')->nullable()->after('locker_address_detail');
            $table->integer('locker_number')->default(0)->nullable()->after('operational_hours');
            $table->string('code_pin')->nullable()->after('locker_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->dropColumn('locker_address_detail');
            $table->dropColumn('operational_hours');
            $table->dropColumn('locker_number');
            $table->dropColumn('code_pin');

        });
    }
}
