<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTransactionItemsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_items', function (Blueprint $table) {
            $table->integer('item_reference',false,false)->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->float('price',8,2)->default(0)->change();
            $table->float('paid_amount',8,2)->default(0)->change();
            $table->float('promo_amount',8,2)->default(0)->change();
            $table->float('refund_amount',8,2)->default(0)->change();
            $table->string('remarks',256)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_items', function (Blueprint $table) {
            //
        });
    }
}
