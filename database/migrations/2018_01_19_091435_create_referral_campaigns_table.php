<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('campaign_name',256);
            $table->text('description')->nullable()->comment('Description will appear on Referral Profile');
            $table->text('description_to_shared')->nullable()->comment('Description will appear on share link/share referral');
            $table->string('type',32)->default('register')->comment("Type Referral: after 'register', 'topup','order'");
            $table->text('rule')->nullable()->comment('Rule for referral, json');
            $table->string('code',32)->nullable()->comment('Special Code referral, for special code promo');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->smallInteger('status',false,false);
            $table->double('from_amount',8,2)->default(0)->comment('From Member Amount, for user who have referral code');
            $table->double('to_amount',8,2)->comment('To Member Amount, for new user / member who submitted referral');
            $table->integer('expired',false,false)->nullable()->comment('number of days expired after submited');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_campaigns');
    }
}
