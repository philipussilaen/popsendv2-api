<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id', false, true);
            $table->integer('available_parameter_id', false, true);
            $table->string('operator')->nullable()->default('=');
            $table->text('value');
            $table->timestamps();

            $table->foreign('campaign_id')->references('id')->on('campaigns');
            $table->foreign('available_parameter_id')->references('id')->on('available_parameters');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_parameters');
    }
}
