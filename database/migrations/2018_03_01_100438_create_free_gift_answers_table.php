<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreeGiftAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_gift_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('free_gift_id',false,true);
            $table->integer('question_id', false, true);
            $table->timestamps();

            $table->foreign('free_gift_id')->references('id')->on('free_gifts');
            $table->foreign('question_id')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('free_gift_answers');
    }
}
