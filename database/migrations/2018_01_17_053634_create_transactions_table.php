<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id',false,true);
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->integer('transaction_id_reference');
            $table->string('transaction_type');
            $table->text('description');
            $table->string('total_price',45);
            $table->string('status',45);
            $table->string('paid_amount',45);
            $table->string('promo_amount',45);
            $table->string('refund_amount',45);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
