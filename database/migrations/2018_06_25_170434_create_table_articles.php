<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('notification_title');
            $table->string('notification_subtitle');
            $table->string('title');
            $table->string('subtitle');
            $table->string('content');
            $table->string('promo_code');
            $table->string('other_desc');
            $table->string('image_locker');
            $table->string('image_web');
            $table->string('image_mobile');
            $table->string('article_service_id');
            $table->string('article_type_id');
            $table->string('article_status_id');
            $table->string('article_priority');
            $table->string('article_country_id');
            $table->string('start_date');
            $table->string('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
