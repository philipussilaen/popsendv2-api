<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSafes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popsafes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id',false,true);
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('locker_name',225);
            $table->string('locker_address',225);

            $table->string('latitude');
            $table->string('longitude');
            $table->string('locker_size')->comment('Default: small,medium,large');
            $table->dateTime('transaction_date');
            $table->dateTime('expired_time');
            $table->string('invoice_code',225);
            $table->string('status',45);
            $table->text('notes')->nullable();
            $table->text('item_photo');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('popsafes');
    }
}
