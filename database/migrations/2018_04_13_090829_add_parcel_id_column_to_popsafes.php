<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParcelIdColumnToPopsafes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->string('parcel_id',256)->nullable()->after('express_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->dropColumn('parcel_id');
        });
    }
}
