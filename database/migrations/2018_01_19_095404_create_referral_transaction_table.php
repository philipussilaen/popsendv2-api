<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('referral_campaign_id',false,true);
            $table->string('code',32)->comment('code submitted by new member');
            $table->integer('from_user_id',false,true)->nullable()->comment('user id who have referral code, null if use special code');
            $table->integer('to_user_id',false,true)->nullable()->comment('new user who submitted referral code');
            $table->string('status',32)->default('PENDING')->comment('referral transaction status. PENDING: waiting customer passed the rule. FAILED: user failed pass the rule. USED: Already Used, pass the rule');
            $table->dateTime('submit_date')->comment('Submit date code');
            $table->dateTime('used_date')->comment('Used / Failed date');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('referral_campaign_id')->references('id')->on('referral_campaigns');
            $table->foreign('from_user_id')->references('id')->on('users');
            $table->foreign('to_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_transactions');
    }
}
