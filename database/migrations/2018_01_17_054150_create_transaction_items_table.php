<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('transaction_id',false,true);
            $table->foreign('transaction_id')->references('id')->on('transactions');

            $table->integer('item_reference');
            $table->text('description');
            $table->string('price',45);
            $table->string('paid_amount',45);
            $table->string('promo_amount',45);
            $table->string('refund_amount',45);
            $table->string('status',45);
            $table->string('remarks',45);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_items');
    }
}
