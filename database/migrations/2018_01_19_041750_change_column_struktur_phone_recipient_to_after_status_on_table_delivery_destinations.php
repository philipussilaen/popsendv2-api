<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnStrukturPhoneRecipientToAfterStatusOnTableDeliveryDestinations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE delivery_destinations MODIFY COLUMN phone_recipient VARCHAR(255) AFTER status");
        DB::statement("ALTER TABLE delivery_destinations MODIFY COLUMN name_recipient VARCHAR(255) AFTER phone_recipient");
        DB::statement("ALTER TABLE delivery_destinations MODIFY COLUMN category VARCHAR(255) AFTER name_recipient");
        DB::statement("ALTER TABLE delivery_destinations MODIFY COLUMN notes VARCHAR(255) AFTER category");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE delivery_destinations MODIFY COLUMN phone_recipient VARCHAR(255) AFTER updated_at");
        DB::statement("ALTER TABLE delivery_destinations MODIFY COLUMN name_recipient VARCHAR(255) AFTER phone_recipient");
        DB::statement("ALTER TABLE delivery_destinations MODIFY COLUMN category VARCHAR(255) AFTER name_recipient");
        DB::statement("ALTER TABLE delivery_destinations MODIFY COLUMN notes VARCHAR(255) AFTER category");
    }
}
