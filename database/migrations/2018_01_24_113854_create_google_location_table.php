<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoogleLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('district',128)->comment('district name');
            $table->string('city',128)->comment('city name');
            $table->string('province',128)->comment('Province name');
            $table->integer('district_id',false,true)->nullable()->comment('Reference to District Table');
            $table->timestamps();

            $table->foreign('district_id')->references('id')->on('districts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_locations');
    }
}
