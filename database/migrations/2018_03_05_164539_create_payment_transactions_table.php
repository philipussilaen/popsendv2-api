<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference');
            $table->string('service');
            $table->string('method');
            $table->string('phone');
            $table->string('country');
            $table->string('description');
            $table->double('amount')->nullable();
            $table->double('paid_amount')->nullable();
            $table->double('total_amount')->nullable();
            $table->string('status')->nullable();
            $table->text('data_request')->nullable();
            $table->text('data_response')->nullable();
            $table->date('expired_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transactions');
    }
}
