<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiredDateToReferralTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('referral_transactions', function (Blueprint $table) {
            $table->dateTime('expired_date')->after('submit_date')->nullable();
            $table->dateTime('used_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referral_transactions', function (Blueprint $table) {
            $table->dropColumn('expired_date');
            $table->dateTime('used_date')->nullable()->change();
        });
    }
}
