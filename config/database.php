<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 23/01/2018
 * Time: 15.00
 */

return [
    'default' => 'mysql',
    'migrations' => 'migrations',
    'connections' => [
        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST'),
            'database' => env('DB_DATABASE'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'express' => [
            'driver' => 'mysql',
            'host' => env('DB_EXPRESS_HOST'),
            'database' => env('DB_EXPRESS_DATABASE'),
            'username' => env('DB_EXPRESS_USERNAME'),
            'password' => env('DB_EXPRESS_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'popbox_db' => [
            'driver' => 'mysql',
            'host' => env('DB_POPBOX_HOST'),
            'database' => env('DB_POPBOX_DATABASE'),
            'username' => env('DB_POPBOX_USERNAME'),
            'password' => env('DB_POPBOX_PASSWORD'),
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_general_ci',
        ]
    ]
];