<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/*Route without User Session*/
$router->group(['prefix'=>'user'],function () use ($router){
    $router->post('check','UserController@getCheckMember');
    $router->post('register','UserController@postRegister');
    $router->post('login','UserController@postLogin');
    $router->post('forgot-password','UserController@forgotPassword');
    $router->get('changePwd/{urlCode}','UserController@getUpdatePassword');
    $router->post('changePwd/{urlCode}','UserController@postUpdatePassword');
    $router->post('update-password','UserController@updatePassword');

    $router->post('login-socialmedia','UserController@loginSocialMedia');
    $router->post('register-socialmedia','UserController@registerSocialMedia');
});
$router->get('pages/{url}','PageController@getPage');
$router->group(['prefix' => 'popsafe'], function () use ($router) {
    $router->post('updateStatus', 'OrderPopsafeController@callbackLocker');
});

$router->get('login','SocialLoginController@redirectToProvider');
$router->get('authuser','SocialLoginController@handleProviderCallback');

$router->group(['prefix'=>'referral'],function () use ($router){
    $router->post('checkReferral','ReferralController@checkReferralCode');
});
$router->post('update-data','OrderController@updateDataDistrict');
$router->post('lockers','LockerController@getLockerLocation');
$router->post('lockerByLockerId','LockerController@getLockerByLockerId');
$router->post('lockersDetail','LockerController@getLockerDetail');

$router->post('notification','NotificationController@sendPushNotification');

$router->group(['prefix' => 'payment'], function () use ($router) {
    $router->post('callbackFixed', 'PaymentController@callbackFixedPayment');
});

$router->group(['prefix'=>'delivery'],function () use ($router){
   $router->post('expressCallback','OrderController@expressCallback');
});

$router->group(['prefix' => 'popsafe-locker'], function () use ($router){
    $router->post('order','OrderPopsafeController@lockerOrder');
    $router->post('extend','OrderPopsafeController@extendPopSafeLocker');
    $router->post('block','LockerController@blockUnBlockParcel');
});
    
$router->post('deduct','PopBoxWalletController@deduct');

/*Route with User Session*/
$router->group(['middleware'=>'auth'],function() use($router){
    $router->post('products','ProductController@index');

    $router->post('categoryList','OrderPopsafeController@getCategoryList');

    $router->group(['prefix'=>'user'],function () use ($router){
        $router->post('edit-profile','UserController@postEditProfile');
        $router->post('get-referral-code', 'UserController@getReferralCode');
        $router->post('checkFcm','UserController@checkFcmCode');
        $router->post('orderHistory','UserController@orderHistory');
        $router->post('orderHistoryDetail','UserController@orderHistoryDetail');
    });

    //Order PopSend
    $router->group(['prefix' => 'popsend'], function () use ($router){
        $router->post('calculate','OrderController@popSendCalculate');
        $router->post('submit','OrderController@popSendSubmit');
        $router->post('history','OrderController@popSendHistory');
        $router->post('history-detail','OrderController@popSendHistoryDetail');

        $router->group(['prefix'=>'1.1'],function () use ($router){
           $router->post('calculate','v1_1\OrderController@popSendCalculate');
            $router->post('submit','v1_1\OrderController@popSendSubmit');
        });
    });

    $router->group(['prefix' => 'transaction'], function () use ($router){
        $router->post('history','TransactionController@HistoryTransaction');
    });

    //Oder PopSafe
    $router->group(['prefix' => 'popsafe'], function () use ($router){
        $router->post('post-order','OrderPopsafeController@postOrder');
        $router->post('cancel-order','OrderPopsafeController@cancelOrder');
        $router->post('calculate','OrderPopsafeController@popSafeCalculate');
        $router->post('history','OrderPopsafeController@popSafeHistory');
        $router->post('historyDetail','OrderPopsafeController@popSafeHistoryDetail');
        $router->post('extend','OrderPopsafeController@extendPopSafe');
        $router->post('refund-order','OrderPopsafeController@refundOrder');
        $router->post('csr-order','OrderPopsafeController@csrOrder');
    });

    //Free Gift
    $router->group(['prefix' => 'freegift'], function () use ($router) {
        $router->post('post-category', 'FreeGiftController@postCategory');
        $router->post('get-category', 'FreeGiftController@getCategory');
        $router->post('get-questions', 'FreeGiftController@getQuestions');
        $router->post('get-merchandises', 'FreeGiftController@getMerchandises');
        $router->post('post-answer', 'FreeGiftController@postAnswer');
        $router->post('get-history','FreeGiftController@getHistory');
    });

    // Payment
    $router->group(['prefix' => 'payment'], function () use ($router) {
        $router->post('getAvailableMethod', 'PaymentController@getAvailableMethod');
        $router->post('checkPayment', 'PaymentController@checkPayment');
        $router->post('createPayment', 'PaymentController@createPayment');
        $router->post('getListPayment','PaymentController@getListPayment');
        $router->post('getAvailableNominal', 'PaymentController@getAvailableNominal');
    });

    // Article
    $router->post('article','ArticleController@getArticle');
    $router->group(['prefix'=>'article'],function () use ($router){
        $router->post('locker','ArticleController@getArticleLocker');
        $router->post('web','ArticleController@getArticleWeb');
        $router->post('mobile','ArticleController@getArticleMobile');
    });

    // Partner
    $router->group(['prefix'=>'partner'],function () use ($router){
        $router->post('howto','PartnerController@getPartnerHowTo');
        $router->post('list','PartnerController@getPartnerList');
    });

    // Notification
    $router->group(['prefix'=>'notification'],function () use ($router){
        $router->post('fcm','UserController@apiPushNotificationFCM');
    });
});
